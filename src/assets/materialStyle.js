import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
    layout:{
        display: 'flex',
        flexDirection: 'column',
        minHeight: '100vh'
        },
    container: {
        backgroundColor: theme.palette.background.paper,
        padding: theme.spacing(8, 0, 6),
        borderBottom: '1px solid #eee'
    },
    
    icon:{
        marginRight: theme.spacing(2)
    },
    button: {
        borderRadius: 3,
        disabled: theme.palette.secondary,

    },
    cardGrid:{
        padding: '20px 0',
    },
    card:{
        height:'100%',
        display: 'flex',
        flexDirection: 'column',
    },
    cardContent:{
        flexGrow: 1,
    },
    cardMedia:{
        paddingTop: '56.25%' //16:9
    },
    footer:{
        backgroundColor: theme.palette.background.paper,
        padding: '30px 0',
        borderTop: '1px solid #eee'
    },
    mainChildren: {
        flexGrow: 1,
    },
    formContainer: {
        marginTop: theme.spacing(3)
    },
    outlinedInput: {
        marginTop: theme.spacing(2)
    },
    formTextField: {
        display: 'flex',
        flexDirection: 'column',
    },
    loading: {
        padding: '50px',
    
    },
    allocateBtn :{
        display: 'flex',
        justifyContent: 'space-between',
        marginTop: '50px'
    },
    volunteers :{
        marginTop:'50px;'
    }

}));

export default useStyles;
