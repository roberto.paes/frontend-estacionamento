import React, { useEffect, useState, useCallback } from "react";
import { useParams } from "react-router";
import {getEstacionamentoDetalheAction,getEstacionamentoAllAction} from "../store/estacionamento/estacionamento.action"
import Tabelainscrito from "../componentes/tabela/index";
import BootstrapTable from 'react-bootstrap-table-next';
import { useDispatch, useSelector } from 'react-redux';
import { estados as estadoList} from '../utils/estados';
import { cidades as cidadeList} from '../utils/cidades';
import { Breadcrumb, BreadcrumbItem } from 'reactstrap';
import { Link } from 'react-router-dom'
import { useHistory } from "react-router-dom";

import {
  Form, FormGroup, Input,
  Card, Col, CardBody,
  CardHeader,
  Button, CardFooter,Label, Alert, Spinner
} from 'reactstrap';
const Detalhes = (props) => {
  const dispatch = useDispatch();
  
  var { estado,cidade,bairro } = useParams();
  var listAll = false
  if(props.estado){
    estado = props.estado
    cidade = props.cidade
    if(estado === "Todos"){
      listAll = true
    }
  }
  let history = useHistory();
  const [loading, setLoading] = useState(false);
  const [detalhe, setDetalhe] = useState([]);
  // use Effect é o ciclo de vida que executa antes* de renderizar a pagina.
  const isLogged = useSelector(state => state.auth.usuario)
  const estacionamento = useSelector(state => state.estacionamento.estacionamentos)
  const loadingEst = useSelector(state => state.estacionamento.loading)
  
  //console.log('#####est',JSON.stringify(estacionamento))
  
  const getDetalhes = useCallback(async () => {
    try {
      setLoading(true);
      if(!listAll){
        dispatch(getEstacionamentoDetalheAction(estado,cidade,bairro))
      }else{
        dispatch( getEstacionamentoAllAction())
      }
      setLoading(false);
    } catch (error) {
      // hasError(true)
      history.push("/?error=404");
    }
  });
  
  useEffect(() => {
    getDetalhes();
  }, []);
  const columns = [{
    dataField: 'nome',
    text: 'Estacionamento',
    sort: true
  }, {
    dataField: 'vagas',
    text: 'Vagas',
    sort: true
  }
  , {
    dataField: 'bairro',
    text: 'Bairro',
    sort: true
  },{
    dataField: 'estado',
    text: 'Estado',
    sort: true
  },{
    dataField: 'cep',
    text: 'Cep',
    sort: true
  },
  , {
    dataField: 'logradouro',
    text: 'Endereço',
    sort: true
  },
  , {
    dataField: 'tarifa.tarifa',
    text: 'Tarifa',
    formatter:priceFormatter,
    sort: true
  },
  { dataField: "edit", 
  text: "",
  sort: false,
  formatter: Edit,
  headerAttrs: { width: 50 },
  attrs: { width: 50} 
}];
function priceFormatter(cell, row, rowIndex, formatExtraData) { 
  var tarifa = 0
  if(cell){
    tarifa = cell[3].preco
  }
  return parseFloat(tarifa).toLocaleString('pt-br',{style: 'currency', currency: 'BRL'}) +'/hora'
}
const CustomButton = ({row}) =>{
  return (<Button color='primary' style={{margin:'10px'}} onClick={e => {
    history.push('/reserva/'+row.id)}}>Reservar</Button>)
  }
  function Edit(cell, row, rowIndex, formatExtraData) { 
    return ( 
      <>
      < div 
      style={{
        cursor: "pointer",
        display: "flex"
      }}>
      
      <CustomButton row={row}/>
      </div>
      </>
      ); }
      const EstacionamentoTable = () =>{
        if(Object.keys(estacionamento).length == 0){
          return <center><h1>Nenhum estacionamento encontrado.</h1></center> 

        }else{
          return <BootstrapTable keyField='id' data={ estacionamento } columns={ columns }/>

        }
      }
      const EstacionamentoRefresh = () =>{
        console.log('loading',loadingEst)
        if(!loadingEst){
          return (<EstacionamentoTable/>)
        }else{
          return (<center><h1>Carregando estacionamentos...</h1></center>)
        }
        
      }
      return (
        <div>
        <h1>Estacionamentos</h1> 
        {estado && !listAll &&  (
          <Breadcrumb>
          <BreadcrumbItem> <Link onClick={()=> dispatch(getEstacionamentoDetalheAction(estado))} to={`/estacionamento/${estado}`}>{estadoList.find(c => c.Sigla == estado).Nome}</Link></BreadcrumbItem>
          {cidade && (
            <>
            <BreadcrumbItem> <Link onClick={()=> dispatch(getEstacionamentoDetalheAction(estado,cidade))} to={`/estacionamento/${estado}/${cidade}`}>{cidadeList.find(c => c.Nome == cidade).Nome}</Link>  </BreadcrumbItem> 
            {bairro && (
              
              <BreadcrumbItem > <Link onClick={()=> dispatch(getEstacionamentoDetalheAction(estado,cidade,bairro))} to={`/estacionamento/${estado}/${cidade}/${bairro}`}>{bairro}</Link></BreadcrumbItem>
              
              )}
              </>
              
              )}
              </Breadcrumb>
              )}
              
              <EstacionamentoRefresh/>
              
              
              </div>
              );
            };
            export default Detalhes;
            