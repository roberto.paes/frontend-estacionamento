import React from 'react'

// import fotos from "../../src/fotos/prefeitura-do-Rio-1.jpg"



const  Sobre = () => {
  return (
   
   <div className="sobre">
     <h2>Quem somos</h2>  
     <p>A Parking Estacionamentos atua no mercado de gestão de estacionamentos, realizando toda a administração de espaços com fins para a guarda de veículos. Trabalhamos com locais próprios e em parcerias comerciais nas modalidades de prédios comerciais, hotéis, garagens em geral, terrenos, aeroportos, rodoviárias, supermercados, centro de eventos, bancos, hospitais e clínicas.

Gerenciamos estacionamentos em todos os ambientes onde eles são necessários e que tragam melhorias na mobilidade urbana, oferecendo soluções de excelência, inovadoras e sustentáveis de acordo com cada projeto. Nos posicionamos com um viés tecnológico para agilizar e facilitar o fluxo em nossas unidades, onde os nossos clientes e parceiros saibam que nos preocupamos não só em guardar os seus veículos em segurança, mas também proporcionar um ótimo atendimento a todos.

 </p>
 {/* <img src="../fotos/prefeitura-do-Rio-1.jpg"></img> */}
   </div>
   

   
  )
}
export default Sobre;