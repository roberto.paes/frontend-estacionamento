

import Estacionamentos from "./estacionamento"
import { estados } from '../utils/estados';
import { cidades } from '../utils/cidades';
import Select from 'react-select'
import { useDispatch, useSelector } from 'react-redux';

import React, { useEffect, useState, useCallback } from "react";
import {
  Form, FormGroup, Input,
  Card, Col, CardBody,
  CardHeader,
  Button, CardFooter, Label, Alert, Spinner
} from 'reactstrap';

var options = {
  enableHighAccuracy: true,
  timeout: 5000,
  maximumAge: 0,
};
function success(pos) {
  var crd = pos.coords;
  
  console.log("Your current position is:");
  console.log(`Latitude : ${crd.latitude}`);
  console.log(`Longitude: ${crd.longitude}`);
  console.log(`More or less ${crd.accuracy} meters.`);
}

function errors(err) {
  console.warn(`ERROR(${err.code}): ${err.message}`);
}



const  Inicio = (props) => {
  const estacionamentos = useSelector(state => state.estacionamento.estacionamentoCountry)
  
  if (navigator.geolocation) {
    navigator.permissions
    .query({ name: "geolocation" })
    .then(function (result) {
      if (result.state === "granted") {
        console.log(result.state);
        //If granted then you can directly call your function here
        navigator.geolocation.getCurrentPosition(success);
      } else if (result.state === "prompt") {
        navigator.geolocation.getCurrentPosition(success, errors, options);
      } else if (result.state === "denied") {
        //If denied then you have to show instructions to enable location
      }
      result.onchange = function () {
        console.log(result.state);
      };
    });
  } else {
    alert("Sorry Not available!");
  }
  const remapEstados = estacionamentos.map((item)=>{
    var stateEst = estados.find((t)=> t.Sigla === item.estado)
    return {tipo:'estado',label:stateEst.Nome,value:stateEst.Sigla,id:stateEst.ID}
  })
  remapEstados.push({tipo:'estado',label:'Todos',value:'Todos',id:'-1'})
  remapEstados.reverse()
  const remapCidades = () => {
    var teste = []    
    estacionamentos.map((item)=>{
      for(var i = 0 ; i < item.cidades.length; i++){
        var stateEst = cidades.find((t)=> t.Nome === item.cidades[i].municipio)
        teste.push({tipo:'cidade',label:stateEst.Nome,value:stateEst.Nome,estado:stateEst.Estado})
      }
    })
    return teste
  }
  remapCidades().push({tipo:'cidade',label:'Todos',value:'Todos',estado:'-1'})
  // const remapCidades = cidades.map((item)=>{
  //   return {tipo:'cidade',label:item.Nome,value:item.Nome,estado:item.Estado}
  // })
  
  console.log('cidades',JSON.stringify(remapCidades()))
  
  //remapEstados.find((t)=> t.value === 'RJ')
  const [estado,setEstado] = useState(remapEstados.find((t)=> t.value === 'Todos'))
  
  const [state, setState] = useState({estado:'Todos'})
  const DateSelector = () => {
    
    const handleEstados = (props) => {
      // console.log('chamge',props)
      state[props.tipo] = props.value
      switch(props.tipo){
        case 'estado':
        setEstado(props)
        break
      }
      console.log('props',props)
      setState({...state},state);
    }
    return(
      <>
      <FormGroup>
      <Label for="estado">Estado:</Label>
      <Select
      placeholder="Selecionar"
      options={remapEstados}
      onChange={handleEstados}
      value={remapEstados.filter(option => option.value === state['estado'])}
      />
      </FormGroup>
      <FormGroup>
      <Label for="cidade">Cidade:</Label>
      <Select
      placeholder="Selecionar"
      options={remapCidades().filter((item)=> item.estado === estado.id)}
      onChange={handleEstados}
      value={remapCidades().filter(option => option.value === state['cidade'])}
      />
      </FormGroup>
      </>
      )
      
    } 
    const EstaciomanetoUpdater = () =>{   
      return(<Estacionamentos estado={estado.value} cidade={ state['cidade']}/>)
      
    }
    return (
      <>
      <DateSelector/>
      <EstaciomanetoUpdater/>
      
      </>
      )
    }
    export default Inicio;