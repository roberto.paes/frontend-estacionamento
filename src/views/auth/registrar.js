import React, { useState } from 'react';
import {
    Form, FormGroup, Input,
    Card, Col, CardBody,
    Button, CardHeader, CardFooter, Label, Alert, Spinner
} from 'reactstrap';
import { Sign } from '../../assets/styled';
import { Link } from 'react-router-dom';
import { formatFactory } from './../../utils/formatFactory';
import { checkCpf } from './../../utils/cpfChecker';
import { signUpAction ,signInAction } from '../../store/auth/auth.action'
import { useDispatch, useSelector } from 'react-redux';
import { useEffect } from 'react';
import useStyles from '../../assets/materialStyle';
import { goToAnchor } from 'react-scrollable-anchor'

const SignUp = () => {
    const classes = useStyles();
    
    const [isValidPassword, setValidPassword] = useState(false)
    const [isValidCpf, setValidCpf] = useState(false)
    const [messageError, setMessageError] = useState('Campos de senha diferentes.')
    const [hasError, setHasError] = useState(false)
    const [success, showSuccess] = useState(false)
    const dispatch = useDispatch();
    const loading = useSelector(state => state.auth.loading)
    const error = useSelector(state => state.auth.error)
    const registered = useSelector(state => state.auth.registered)
    const [senha, setSenha] = useState({ senha: '', senharepeat: '' })
    const data = {
        "username":"teshte2444",
        "senha":"123",
        "nome":"admin da silva paes" ,
        "email":"theste4444@example.com",
        "cpf":"00000h000",
        "datanascimento":"04/14/2021",
        "tipo":1
    }
    const [form, setForm] = useState({
        nome: '',
        email: '',
        datanascimento: '',
        senha: '',
        username: '',
        cpf: ''
    })
    //    const [form, setForm] = useState(data)
    const handleChange = (props) => {
        const { value, name } = props.target;
        form[name] = value
        setForm({...form},form);
    };
    
    const handleTipo = (props) =>{
        const { value, name } = props.target;
        form['tipo'] = value
        setForm({...form},form);
        
    }
    const handleCpf = (props) => {
        const { value, name } = props.target;
        form[name] = formatFactory(value, 'xxx.xxx.xxx-xx')
        setValidCpf(checkCpf(form[name]))
        setForm({...form},form);
    };
    const handlePassword = (props) => {
        const { value, name } = props.target;
        senha[name] = value
        setSenha(senha);
        if (value == senha['senha'] && value == senha['senharepeat']) {
            form['senha'] = value
            setForm(form);
            setValidPassword(true)
        } else {
            setMessageError("Senha inválida!")
            setValidPassword(false)
        }
        
    }
    const closeError = () => setHasError(false);
    const submitForm = (event) => {
        var cpfnonformated = form['cpf'].replace(/[^\d]/g, "")
        const nForm = {
            ...form,
            ['cpf']: cpfnonformated
        }
        
        dispatch(signUpAction(nForm))
    }
    const isNotValid = () => {
        const inputs = Object.keys(form)
        const invalid = (label) => !Object.keys(form).includes(label) || form[label].length === 0
        
        if (!isValidPassword || !isValidCpf) {
            return true
        }
        return inputs.some(item => invalid(item))
    }
    useEffect(() => {
        setHasError(error.length > 0)
        if(error.length > 0){
            setMessageError(error[0].error)
            goToAnchor('message')
            
        }
    }, [error])
    useEffect(() => {
        if (registered) {
            showSuccess(true)
            goToAnchor('message')
            
            setTimeout(()=>{
                dispatch(signInAction({email:form.email,senha:form.senha}))
                setForm({})
                setSenha({})
            },5000)
            
        }
    }, [registered])
    
    
    return (
        <Sign>
        <Col sm={12} md={4} lg={5}>
        <div id="message">
        <Alert color="success" isOpen={success} toggle={() => showSuccess(!success)}>
        <div><strong>Cadastrado com sucesso.</strong><br/>Dentro de cinco segundos, você será redirecionado para o painel.</div>
        </Alert>
        <Alert color="danger" isOpen={hasError} toggle={closeError}>
        <div><strong>OPS !!! </strong> Aconteceu um erro.</div>
        <small>{messageError}</small>
        </Alert>
        </div>
        <Card>
        <CardHeader tag="h4" className="text-center">Cadastre-se</CardHeader>
        <CardBody>
        <Form>
        <FormGroup>
        <Label for="nome">Nome:</Label>
        <Input disabled={loading} type="text" name="nome" id="nome" onChange={handleChange} value={form.nome || ""} placeholder="Informe seu Nome" />
        </FormGroup>
        <FormGroup>
        <Label for="username">Usuário:</Label>
        <Input disabled={loading} type="text" name="username" id="username" onChange={handleChange} value={form.username || ""} placeholder="Informe seu Usuário" />
        </FormGroup>
        <FormGroup>
        <Label for="email">E-mail:</Label>
        <Input disabled={loading} type="email" name="email" id="email" onChange={handleChange} value={form.email || ""} placeholder="Informe seu E-mail" />
        </FormGroup>
        <FormGroup>
        <Label for="datanascimento">Data de Nascimento:</Label>
        <Input disabled={loading} type="date" name="datanascimento" id="datanascimento" onChange={handleChange} value={form.datanascimento || ""} placeholder="Informe sua data de nascimento" />
        </FormGroup>
        <FormGroup check>
        <Input type="radio" name="tipo" onChange={handleTipo} value="0"/>
        Eu preciso de um estacionamento
        <Label check>
        <Input type="radio" name="tipo" onChange={handleTipo} value="1"/>
        Eu tenho um estacionamento
        </Label>
        </FormGroup>
        <Alert color="warning" isOpen={!isValidCpf} toggle={closeError}>
        <small>Cpf inválido.</small>
        </Alert>
        <FormGroup>
        <Label for="cpf">Cpf:</Label>
        <Input disabled={loading} type="text" name="cpf" id="cpf" onChange={handleCpf} value={form.cpf || ""} placeholder="Informe seu Cpf" />
        </FormGroup>
        <Alert color="warning" isOpen={!isValidPassword} toggle={closeError}>
        <small>{messageError}</small>
        </Alert>
        <FormGroup>
        <Label for="password">Senha:</Label>
        <Input disabled={loading} type="password" name="senha" id="senha" onChange={handlePassword} placeholder="Informe sua senha" />
        </FormGroup>
        <FormGroup>
        <Label for="senharepeat">Repita sua senha:</Label>
        <Input disabled={loading} type="password" name="senharepeat" id="senharepeat" onChange={handlePassword} placeholder="Repita sua senha" />
        </FormGroup>
        <Button color={isNotValid() || loading ? 'secondary' : 'primary'} disabled={isNotValid()} size="sm" block onClick={submitForm}>
        {loading ? (<><Spinner size="sm" color="light" /> Carregando...</>) : "Cadastrar"}
        </Button>
        

    </Form >
    </CardBody>
    <CardFooter className="text-muted">
    Já tem acesso ? <Link to="/login">Faça o Login</Link>
    </CardFooter>
    
    </Card>
    </Col>
    </Sign>
    
    )
}

export default SignUp;

