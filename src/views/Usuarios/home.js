import React, { useEffect, useState, useCallback } from "react";
import { useParams } from "react-router";
import {getUser} from "../../services/user.service";
import Time from '../../componentes/date'
import {
  Form, FormGroup, Input,
  Card, Col, CardBody,
  CardHeader,
  Button, CardFooter,Label, Alert, Spinner
} from 'reactstrap';
import { useDispatch, useSelector } from 'react-redux';
import Modal from 'react-bootstrap/Modal'
import store from '../../store';
import {updateUsuarioction,refreshUsuarioAction} from "../../store/usuario/usuario.action";
import HomeModel from '../../componentes/Usuario/Home/model'
import { saveUser } from "../../config/auth";

const Perfil = (props) => {
  const { id } = useParams();
  const dispatch = useDispatch();
  const [modal, setModal] = useState(false);
  const state = store.getState();

  const { history } = props;
  const [loading, setLoading] = useState(false);
  const [detalhe, setDetalhe] = useState({});
  // use Effect é o ciclo de vida que executa antes* de renderizar a pagina.
  const handleCancel= () => setModal(false)
  
  const handleClose = () => setModal(false);
  const handleShow = () => setModal(true) 

  const getDetalhes = useCallback(async () => {
    try {
      setLoading(true);
      const res = await getUser();
      setDetalhe(res.data);
      setLoading(false);
    } catch (error) {
      
      // hasError(true)
      history.push("/?error=404");
    }
  }, [id, history]);
  
  useEffect(() => {
    getDetalhes();
  }, [getDetalhes]);
  const Carros = ({carros}) => {
    if(carros){
      if(carros.length == 1){
        return <h5>Você tem {carros.length} carro cadastrado.</h5>
      }else{
        return <h5>Você tem {carros.length} carros cadastrados.</h5>
        
      }
    }else{
      return <h5>Você não possui carros cadastrados.</h5>
    }
    
  }
  const Reservas = ({reservas}) => {


    var today = false
    var datetoday = new Date().toLocaleDateString("pt-BR", { // you can use undefined as first argument
      year: "numeric",
      month: "2-digit",
      day: "2-digit",
    });
    
    if(reservas && reservas.length > 0){
      var reserv_count = 0
      var entradahour = reservas.map((item)=>{
        var entrada = new Date(item.entrada.split('.')[0])
        var today = new Date()
        if(entrada.getDate() === today.getDate() && entrada.getMonth() === today.getMonth() && entrada.getFullYear() === today.getFullYear() && entrada > today){
          console.log('testtt', true)
          reserv_count++
          return <div>{item.estacionamento.nome}<Time date={item.entrada}/></div> 
        }
        
      })
      
      
      //  return <h1>Aviso: Você tem uma reserva para hoje.<h4> Em {datetoday} às {entradahour}</h4></h1>
      if(entradahour){
        if(reserv_count == 1){
          return (
            <div>
            
            <h1>Você tem uma reverva para hoje:</h1>
            <h4>{entradahour}</h4></div>)
          }else if(reserv_count > 1){
            return (
              <div>
              
              <h1>Você tem {reserv_count} revervas para hoje:</h1>
              <h4>{entradahour}</h4></div>)
            }
          }
          
          
          if(reservas.length == 1){
            return <h5>Você tem {reservas.length} reserva cadastrada.</h5>
          }else{
            return <h5>Você tem {reservas.length} reservas cadastradas.</h5>
            
          }
          
          
        }else{
          return <h1>Você não possui reservas cadastradas.</h1>
        }
        
      }
            
      const Salvar = () =>{
        
        
         dispatch(updateUsuarioction(state.usuario.usuarioCache))
        // setTimeout(() => {
        //   dispatch(refreshUsuarioAction())

        // },250)
         
        handleClose()
      }

      const CustomModel = () =>{
        const canDoIt = useSelector(state => state.usuario.isValid)

        return <Modal show={modal} onHide={handleClose} scrollable={true} size="lg"
        dialogClassName='custom-dialog'
        centered>
        <Modal.Header closeButton>
        <Modal.Title>Editor de perfil</Modal.Title>
        </Modal.Header>
        <Modal.Body><HomeModel/></Modal.Body>
        <Modal.Footer>
        <Button color="danger" onClick={handleCancel}>
        Cancelar
        </Button>
        <Button color={canDoIt|| state.usuario.loading ? 'secondary' : 'success'} disabled={canDoIt} onClick={Salvar}>
{state.usuario.loading ? (<><Spinner size="sm" color="light" /> Carregando...</>) : "Salvar"}
</Button>
        
        </Modal.Footer>
        </Modal>
        
      }
      return (
        <div>
     
             <Button size="sm" onClick={() => handleShow()} color="primary">
   Editar minhas informações
    </Button>
    <br/>
    <CustomModel/>
        Olá, {detalhe.nome}
     
        <Carros carros={detalhe.carros}/> 
        <Reservas reservas={detalhe.reservas}/>
        </div>
        );
      };
      export default Perfil;
      