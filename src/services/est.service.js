import http from '../config/http';

//routes get
const getEstacionamentosList= () => http.get('/v1/estacionamento/lista')
//const getEstacionamentoDetalhe = (estado,cidade,bairro) => http.get(`/estacionamento/lista/${estado}/${cidade}/${bairro}`)
const getEstacionamentoId = (id) => http.get(`/v1/estacionamento/${id}`)
const getEstacionamentoAll = () => http.get(`/v1/estacionamento/get/all`)
//nivelar usuario
const getEstacionamento= () => http.get(`/v1/estacionamento`)
//routes post
const postEstacionamento= (data) => http.post('/v1/estacionamento/insert',data)
const updateEstacionamento= (data) => http.put('/v1/estacionamento/update',data)
const delEstacionamento= (post) => http.delete('/v1/estacionamento/del', {data:post} )
const getsearchEstacionamento= (cep) => http.get(`/v1/estacionamento/search/distance/${cep}`)


export {
   getEstacionamentosList,
   getEstacionamentoAll,
   getsearchEstacionamento,
   delEstacionamento,
   updateEstacionamento,
   postEstacionamento,
   getEstacionamentoId,
   getEstacionamento
}