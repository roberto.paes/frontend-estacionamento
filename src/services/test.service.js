import http from '../config/http';

const authTest = (post) => http.put(`/v2/test/auth`, post)

export  {
    authTest
}