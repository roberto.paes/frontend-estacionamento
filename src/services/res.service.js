import http from '../config/http';
const postReservaInsert = (post) => http.post(`/v1/reserva/insert`, post)
const updateReserva = (post) => http.put(`/v1/reserva/update`, post)
const delReserva = (post) => http.delete(`/v1/reserva/del`, post)
const getReserva = () => http.get(`/v1/reserva`)
const getReservaId = (id) => http.get(`/v1/reserva/${id}`)
export {
     getReserva,
     getReservaId,
     delReserva,
     updateReserva,
     postReservaInsert
     
}