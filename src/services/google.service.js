import axios from 'axios'; // import da dependencia

const getCep = (estado,localidade,endereco) => axios.get(`https://viacep.com.br/ws/${estado}/${localidade}/${endereco}/json/`)
export {
    getCep
}