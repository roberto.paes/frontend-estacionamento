import http from '../config/http';


const authLogin = (post) => http.post(`/v1/usuario/login`, post )
const getUser = () => http.get(`/v1/usuario`)
const authRegister = (post) => http.post(`/v1/usuario/insert`, post)
const authUpdate = (post) => http.put(`/v1/usuario/update`, post)

export  {
     authLogin,
     authUpdate,
     getUser,
     authRegister 
}