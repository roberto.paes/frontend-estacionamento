import http from '../config/http';


const getCarro = (id) => http.get(`/v1/carro/${id}`)
const getCarros = () => http.get(`/v1/carro`)
const editCarro = (post) => http.put(`/v1/carro/update`, post )
const deleteCarro = (post) => http.delete(`/v1/carro/del`, {data:post} )
const postCarro = (post) => http.post(`/v1/carro/insert`, post)

export  {
   getCarro,
   getCarros,
   editCarro,
   deleteCarro,
   postCarro

}