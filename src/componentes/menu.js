
import { Link } from 'react-router-dom'
import { getUser,getToken } from '../config/auth'
import React, { useEffect, useState, setState } from 'react';
import { propTypes, NavDropdown, Navbar, Nav, Dropdown, Form, FormControl, Button } from 'react-bootstrap';
import { useDispatch, useSelector } from 'react-redux';
import { logoutAction } from '../store/auth/auth.action';

export default function Menu({props}) {
  
  const dispatch = useDispatch();

  const logout = () => {
    dispatch(logoutAction())
}
  const PrivateNivel = ({ nivel, ...props }) => {
    var niv = nivel || 0
    if (getUser()?.tipo >= niv) { return <>{props.children}</> } else { return <></> };
  }


  if (getUser()) {
    return <NavDropdown title="Painel" id="basic-nav-dropdown">

      Bem-vindo(a), {getUser().nome.split(' ')[0]}
      <Nav.Link as={Link} to="/home">Home</Nav.Link>
      <Nav.Link as={Link} to="/carros">Meus carros</Nav.Link>
      <Nav.Link as={Link} to="/reservas">Minhas reservas</Nav.Link>
      <PrivateNivel nivel={1}><Nav.Link as={Link} to="/estacionamentos">Meus estacionamentos</Nav.Link></PrivateNivel>

      <Nav.Link as={Link} to="/login" onClick={logout}>Sair</Nav.Link>

    </NavDropdown>

  } else {
    return <Nav.Link as={Link} to="/login">Login</Nav.Link>
  }

}