import './style.css'
import { Link } from 'react-router-dom'
import { getUser } from '../../config/auth'

const Tabela = ({ inscritos }) => {
const CanReserve = ({...props}) => {
    if(getUser()){
        return props.children
    }
    else{
        return <div>É necessário fazer login<br/> para efetuar uma reserva</div>
    }
}

    return (
        <div>
            {inscritos && inscritos.length ? (
                <table border="1">
                    <thead>
                        <tr>
                        <th>Estacionamento</th>
                            <th>Vagas</th>
                            <th>Endereço</th>
                            <th>Bairro</th>
                            <th>Cep</th>                            
                            <th>Tarifa</th> 
                            <th></th>                        
                        </tr>
                    </thead>
                    <tbody>
                        {inscritos && inscritos.map((v, i) => (
                            <tr>
                                <td>{v.nome}</td>
                                <td>{v.vagas}</td>
                                <td>{v.endereco}</td>
                                <td>{v.bairro}</td>
                                <td>{v.cep}</td>
                                <td>{parseFloat(v.tarifa).toLocaleString('pt-br',{style: 'currency', currency: 'BRL'})}</td>
                                <td><CanReserve><Link to={"/reserva/" + v.id}>Reserva</Link></CanReserve></td>
                            </tr>
                        ))}

                    </tbody>
                </table>
            ) : (
                <div>Não existem alunos cadastrados</div>
            )}
        </div>
    )
}

export default Tabela


