import React,{ useState , useEffect } from 'react';
import Header from "./header"
import Footer from "./footer"
import styled from 'styled-components';

import '../../assets/App.scss'
import { useHistory } from "react-router-dom";

const Layout = (props) => {
  const history = useHistory();
  const [title,setTitle] = useState('EASY PARKING')

  document.title = title;

  useEffect(() => {
    return history.listen((location) => { 
      let _title = location.pathname.replace('/', '')
      _title = _title.indexOf('/') > 0 ? _title.substring(0, _title.indexOf('/')) : _title 
      switch(_title.toUpperCase()){
        case '': 
        _title = `${title}-Início`
        break;
        case 'CADASTRO':
        _title = `${title}-Cadastro`
        break; 
        case 'ESTACIONAMENTOS':
        _title = `${title}-Meus estacionamentos`
        break; 
        case 'CARROS':
        _title = `${title}-Meus carros`
        break; 
        case 'RESERVAS':
        _title = `${title}-Minhas reservas`
        break; 
        default:
        _title = `${title}-${_title.charAt(0).toUpperCase() + _title.slice(1)}`
        break; 
      } 
      setTitle(_title)
    }) 
  },[history]) 
  
  return (
    <>
    <Header props={props}/>
    <Main className="container">
    {props.children}
    </Main>
    <Footer title={title} />
    </>
    )
  }
  
  export default Layout;
  
  
  const Main = styled.main`
  flex:1;
  `
  