import React, { useEffect, useState } from 'react';
import {getEstacionamentosList} from '../../services/est.service'
import {searchEstacionamento, getEstacionamentoDetalheAction,getEstacionamentoListaAction} from '../../store/estacionamento/estacionamento.action'
import MenuUser from '../menu'
import { removeToken, removeUser, saveAuth, saveUser,getUser } from "../../config/auth";
import { Link } from 'react-router-dom'
import { propTypes, NavDropdown, Navbar, Nav, Form, FormControl,InputGroup } from 'react-bootstrap';
import {
  Button
} from 'reactstrap';
import { useDispatch, useSelector } from 'react-redux';
import { Dropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap';
import './style.css'
import PlacesAutocomplete, {
  geocodeByAddress
} from 'react-places-autocomplete';
import { useHistory } from "react-router-dom";
import logo from '../../assets/img/logo.png';
import store from './../../store';

export default function Header(props) {
  const dispatch = useDispatch();
  const storeState = store.getState();
  const [address, setAddress] = useState('');
  const history = useHistory();
  
  const [estado, setEstado] = useState([]);
  const isLogged = useSelector(state => state.auth.isLogged)
  const usuario = useSelector(state => state.auth.usuario)
  //const estado = useSelector(state => state.estacionamento.estacionamentocountry)
  
  const navigate = async () => {
    await dispatch(getEstacionamentoListaAction())
    //  var res = await getEstacionamentosList()
    setEstado(storeState.estacionamento.estacionamentoCountry)
  }
  //const [title, setTitle] = useState(""); 
  
  // setar o titulo da pagina
  
  useEffect(() => {
    navigate()
  },[]);
  const handleChange = param => {
    setAddress(param);
  };
  const clearSearh = () =>{
    setAddress('')
  }
  const handleSelect = param => {
    
    const handleCep = async (data) =>{
      console.log('placeid',data['place_id'])
      console.log('data',JSON.stringify(data))
      dispatch(searchEstacionamento(data['place_id']))
      history.push('/estacionamento/finder')
    }
    geocodeByAddress(param).then((results) =>{
      handleCep(results[0])
    }).catch(error => console.error('Error', error));
  };
  const onError = (status, clearSuggestions) => {
    console.log('Google Maps API returned error with status: ', status)
    clearSuggestions()
  }
  return (
    <header >
    <Navbar bg="light" expand="lg">
    <Navbar.Brand href="/"><img src={logo} height="50px" alt="Logo" /></Navbar.Brand>
    <Navbar.Toggle aria-controls="responsive-navbar-nav" />
    <Navbar.Collapse id="responsive-navbar-nav">
    <Nav className="mr-auto">
    <Nav.Link as={Link} to="/">Início</Nav.Link>
    <Nav.Link as={Link} to="/cadastro">Cadastro</Nav.Link>
    <MenuUser />
    <div className="collapse navbar-collapse" id="main_nav">
    <ul className="navbar-nav">
    <li className="nav-item dropdown" id="myDropdown">
    <a className="nav-link dropdown-toggle" href="#" data-bs-toggle="dropdown">  Estacionamento </a>
    
    <ul  className="dropdown-menu">
    {estado.map((item, index) => {   
      return(
        <li key={'e'+index}> <Link className="dropdown-item" onClick={()=> dispatch(getEstacionamentoDetalheAction(item.estado))} to={`/estacionamento/${item.estado}`}>{item.estado}</Link>
        <ul className="submenu dropdown-menu">
        {item.cidades.map((cidade,cidadeindex) => 
          <li key={'c'+cidadeindex} > <Link className="dropdown-item" onClick={()=> dispatch(getEstacionamentoDetalheAction(item.estado,cidade.municipio))} to={`/estacionamento/${item.estado}/${cidade.municipio}`}>{cidade.municipio}</Link>
          <ul  className="submenu dropdown-menu">
          {cidade.bairros.map((bairro,bairroindex) =>
            <li key={'b'+bairroindex}> <Link className="dropdown-item" onClick={()=> dispatch(getEstacionamentoDetalheAction(item.estado,cidade.municipio,bairro))} to={`/estacionamento/${item.estado}/${cidade.municipio}/${bairro}`}>{bairro}</Link></li>
            )}
            </ul>
            </li>
            )}
            </ul>
            </li>
            )})}
            </ul>
            
            </li>
            
            </ul>
            
            </div>
            
            
            </Nav>
            <InputGroup className="mb-3">
           
           <PlacesAutocomplete
           value={address}
           onChange={handleChange}
           onSelect={handleSelect}>
           
           {({ getInputProps, suggestions, getSuggestionItemProps, loading }) => (
             <div>
             
             <input
             
             {...getInputProps({
               placeholder: 'Procurar estacionamentos mais próximos por endereço ...',
               className: 'location-search-input',
               
             })}
             
             />
             
             <div className="autocomplete-dropdown-container">
             
             {loading && <div>Carregando...</div>}
             {suggestions.map((suggestion,index) => {
               const className = suggestion.active
               ? 'suggestion-item--active'
               : 'suggestion-item';
               // inline style for demonstration purpose
               const style = suggestion.active
               ? { backgroundColor: '#fafafa', cursor: 'pointer'  }
               : { backgroundColor: '#ffffff', cursor: 'pointer' };
               return (
                 
                 <div key={index}
                 {...getSuggestionItemProps(suggestion, {
                   className,
                   style,
                 })}
                 >
                 
                 <span>{suggestion.description}</span>
                 
                 </div>
                 );
               })}
               
               </div>
               
               </div>
               
               )}
               
               </PlacesAutocomplete>
               <InputGroup.Append> 
               <InputGroup.Text id="basic-addon2"><Button onClick={() => clearSearh()} close /></InputGroup.Text>
               </InputGroup.Append> 
               </InputGroup> 
     
    
                
      
            
                </Navbar.Collapse>
                
                
                </Navbar>
                
                </header>
                )
              }
              