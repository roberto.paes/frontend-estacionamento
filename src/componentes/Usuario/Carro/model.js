import React, { useEffect, useState, useCallback } from "react";
import Header from "../../Layout/header";
import {getCarro,getCarros,editCarro,deleteCarro,postCarro}  from "../../../services/car.service";
import { useParams } from "react-router";
import { Link } from "react-router-dom";
import { Sign } from '../../../assets/styled';
import { setCarroAction } from '../../../store/carro/carro.action'
import { formatFactory } from '../../../utils/formatFactory'
import store from './../../../store';

import { useDispatch, useSelector } from 'react-redux';
import {
  Form, FormGroup, Input,
  Card, Col, CardBody,
  CardHeader,
  Button, CardFooter, Label, Alert, Spinner
} from 'reactstrap';

export default function Carro({ id }) {
  const stateRedux = store.getState();
  
  const [hasError, setHasError] = useState(false)
  const [HasCritical, setHasCriticalError] = useState(false)
  
  const error = useSelector(state => state.carro.error)
  const [messageError, setMessageError] = useState('')
  const [state, setState] = useState({
    marca: '',
    modelo: '',
    placa:''
  })
  useEffect(() => {
    setHasCriticalError(error.length > 0)
    if(error.length > 0){
      console.log("deu erro sim")
      var cache = stateRedux.carro.carroCache
      if(Object.keys(cache).length > 0){
        setMessageError(error[0].error)
        Object.keys(state).some((item) => {
          state[item] = cache[item] 
        })
        setState({...state},state);
        dispatch(setCarroAction({...state},false))
      }
    }
  }, [error])
  
  const dispatch = useDispatch();
  let jsonReceveid = {}
  const [formValidator, setFormValidator] = useState(
    {
      marca: {
        "status": false,
        "message": "teste"
      },
      modelo: {
        "status": false,
        "message": "teste"
      },
      placa: {
        "status": false,
        "message": "teste"
      }
    }
    )
    const closeError = () => setHasError(false);
    const closeErrorCritical = () => setHasCriticalError(false);  
    
    const isNotValid = () => {
      const inputs = Object.keys(state)
      const invalid = (label) => !Object.keys(state).includes(label) || state[label].length === 0
      var validation = []
      for(var item in formValidator){
        validation.push([formValidator[item]])
      }
      
      var checker = validation.some((item) => item[0]['status'] == true)
      if(checker){
        dispatch(setCarroAction({},true))
        return true
      }else{
        dispatch(setCarroAction({...state},false))
      }
      return inputs.some(item => invalid(item))
    }
    const JsonCorrection = (label) => {
      if(jsonReceveid[label] == null || jsonReceveid[label] == undefined ){
        jsonReceveid[label] = ""
      }
    }
    useEffect( () => {
      const selectCarro = async () =>{
        await getCarro(id).then((sucess)=>{
          jsonReceveid = sucess.data 
          delete jsonReceveid['createdAt']
          delete jsonReceveid['usuarioId']
          delete jsonReceveid['updatedAt']
          delete jsonReceveid['id'] 
          Object.keys(jsonReceveid).some(item => JsonCorrection(item))
          setState(jsonReceveid)
          dispatch(setCarroAction({...jsonReceveid},false))
          
          
        })
      }
      if(id){
        if(!error.length > 0 ){
          selectCarro()
        }
      }
      
    }, []);
    useEffect(() => {
      const timeoutId = setTimeout(() => {
        setHasError(isNotValid())
      }, 1000);
      return () => clearTimeout(timeoutId);
    }, [state,formValidator]);
    const handleValidator = (props) =>{
      const { value, name } = props.target;
      switch(name){
        case 'marca':
        var isString = /^([a-zA-Z0-9]\s*)+$/.test(value)
        setFormValidator({...formValidator,[name]:{status:!isString,message:`Não é possível inserir acentuação em ${name}`}})
        break
        case 'modelo':
        var isString = /^([a-zA-Z0-9]\s*)+$/.test(value)
        setFormValidator({...formValidator,[name]:{status:!isString,message:`Não é possível inserir acentuação em ${name}`}})
        break
        case 'placa':
        var isString = /[A-Z]{3}[0-9][0-9A-Z][0-9]{2}/.test(value)
        setFormValidator({...formValidator,[name]:{status:!isString,message:`Placa inválida em${name}`}})
        break
        
      }
    }
    const handleChange = (props) => {
      const { value, name } = props.target;
      state[name] = value
      setState({...state},state);
      setHasError(isNotValid())
      handleValidator(props)
      if(!isNotValid()){
        dispatch(setCarroAction({...state},false))
      }else{
        dispatch(setCarroAction({},true))
      } 
    }
    
    return (
      <Sign>
      <Col sm={12} md={10} lg={10}>
      <Card>
      <CardHeader tag="h4" className="text-center"></CardHeader>
      <CardBody>
      
      <Alert color="danger" isOpen={hasError} toggle={closeError}>
      <div><strong>OPS !!! </strong> Aconteceu um erro.</div>
      <small>Preencha todos os campos.</small>
      </Alert>
      <Alert color="danger" isOpen={HasCritical} toggle={closeErrorCritical}>
      <div><strong>OPS !!! </strong> Aconteceu um erro.</div>
      <small>{messageError}</small>
      </Alert>
      <Form>
      
      <FormGroup>
      <Alert color="warning" isOpen={formValidator['marca']['status'] || false}>
      <small>{formValidator['marca']['message']}</small>
      </Alert>
      <Label htmlFor="marca">
      </Label>
      <Input
      placeholder="Marca do veículo"
      type="text"
      value={state.marca || ''}
      onChange={handleChange}
      name="marca"
      id="marca"
      />
      </FormGroup>
      <FormGroup>
      <Alert color="warning" isOpen={formValidator['modelo']['status'] || false}>
      <small>{formValidator['modelo']['message']}</small>
      </Alert>    
      <Label htmlFor="modelo">
      
      </Label>
      <Input
      type="text"
      value={state.modelo || ''}
      placeholder="Modelo do veículo"
      onChange={handleChange}
      name="modelo"
      id="modelo"
      />
      </FormGroup>
      <FormGroup>
      <Alert color="warning" isOpen={formValidator['placa']['status'] || false}>
      <small>{formValidator['placa']['message']}</small>
      </Alert>   
      <Label htmlFor="placa">
      </Label>
      <Input
      type="text"
      value={state.placa || ''}
      placeholder="Placa do veículo"
      maxLength="7"
      
      onChange={(props)=>{
        handleChange(props) 
        state['placa'] = state['placa'].toLocaleUpperCase()
        setState({...state})
      }}
      name="placa"
      id="placa"
      />
      </FormGroup>

      
      
      </Form>
      </CardBody>
      
      
      </Card>
      </Col>
      </Sign>
      
      );
    }
    