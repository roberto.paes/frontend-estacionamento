
import { Link } from 'react-router-dom'
import BootstrapTable from 'react-bootstrap-table-next';
import React, { useEffect, useState } from 'react';
import CarModal from './model';

import {
  Form, FormGroup, Input,
  Card, Col, CardBody,
  CardHeader,
  Button, CardFooter,Label, Alert, Spinner
} from 'reactstrap';
import Modal from 'react-bootstrap/Modal'
import { finishCarroAction,deleteCarroAction, updateCarroAction,postCarroAction,refreshCarAction } from '../../../store/carro/carro.action'
import { useDispatch, useSelector } from 'react-redux';
import store from './../../../store';
const TabelaCarros = () => {
  const dispatch = useDispatch();
  const state = store.getState();
  
  const [success, showSuccess] = useState(false)
  const Carros = useSelector(state => state.carro.carros)
  
  
  useEffect(()=>{
    dispatch(refreshCarAction())
  },[])
  const [modal, setModal] = useState(false);
  const [item, setItem] = useState({});
  useEffect(()=>{
    if(!modal){
      dispatch(refreshCarAction())
    }
  },[modal])
  const handleCancel= () => setModal(false)
  
  const handleClose = () => setModal(false);
  const handleShow = (type,row) => {   
    setModal(true)
    dispatch(finishCarroAction())
    
    setItem({type,row})
    // console.log('row',row)
  }
  
  const TYPES = {
    CARRO_UPDATE: "CARRO_UPDATE",
    CARRO_INSERT: "CARRO_INSERT",
    CARRO_DEL: "CARRO_DEL",
  }
  function rankFormatter(cell, row, rowIndex, formatExtraData) { 
    return ( 
      <>
      < div 
      style={{
        cursor: "pointer",
        display: "flex"
      }}>
      
      <Button color="warning" style={{marginLeft:'10px'}}onClick={e => {
        handleShow(TYPES.CARRO_UPDATE,row);
      }}>Editar</Button>
      
      <Button color="danger" style={{marginLeft:'10px'}} onClick={e => {
        handleShow(TYPES.CARRO_DEL,row);
      }}>Deletar</Button>
      </div>
      </>
      ); }
      
      const Title = () => {
        switch(item.type){
          case TYPES.CARRO_UPDATE:
          return  <Modal.Title>Editar carro</Modal.Title>
          case TYPES.CARRO_DEL:
          return  <Modal.Title>Deletar carro</Modal.Title>
          case TYPES.CARRO_INSERT:
          return  <Modal.Title>Cadastrar carro</Modal.Title>
        }
        
      }
      
      const Body = () => {
        
        switch(item.type){
          case TYPES.CARRO_DEL:
          state.carro.isValid = false
          
          return  <Modal.Body>Tem certeza que deseja excluir esse carro? Essa ação não pode ser desfeita.</Modal.Body>
          case TYPES.CARRO_UPDATE:
          return  <Modal.Body><CarModal id={item.row.id}/></Modal.Body>
          case TYPES.CARRO_INSERT:
          return  <Modal.Body><CarModal/></Modal.Body>
        }
      }
      
      const Salvar = async () =>{
        
        
        switch(item.type){
          
          case TYPES.CARRO_UPDATE:
          await  dispatch(updateCarroAction(item.row.id,state.carro.carroCache))
          break;
          case TYPES.CARRO_INSERT:  
          await  dispatch(postCarroAction(state.carro.carroCache))
          break;
          case TYPES.CARRO_DEL:
          await  dispatch(deleteCarroAction(item.row.id))
          break;
        }
        
        if(!state.carro.error.length > 0){
          setModal(false)
        }
        //        dispatch(refreshCarAction())
        
      }
      const columns = [{
        dataField: 'marca',
        text: 'Marca',
        sort: true
      }, {
        dataField: 'modelo',
        text: 'Modelo',
        sort: true
      }, {
        dataField: 'placa',
        text: 'Placa',
        sort: true
      },
      { 
        dataField: "edit", 
        text: "",
        sort: false,
        formatter: rankFormatter,
        headerAttrs: { width: 50 },
        attrs: { width: 50} 
      }
      
    ];
    const CreateEst = () =>{
      return (<Button color="info" onClick={()=>handleShow(TYPES.CARRO_INSERT,-1)} >Cadastrar veículo</Button>)
    }
    
    const CustomModel = () =>{
      const canDoIt = useSelector(state => state.carro.isValid)
      
      return <Modal show={modal} onHide={handleClose} scrollable={true} size="lg"
      dialogClassName='custom-dialog'
      centered>
      <Modal.Header closeButton>
      <Title/>
      </Modal.Header>
      <Body/>
      <Modal.Footer>
      <Button color="danger" onClick={handleCancel}>
      Cancelar
      </Button>
      <Button color={canDoIt|| state.carro.loading ? 'secondary' : 'success'} disabled={canDoIt} onClick={Salvar}>
      {state.carro.loading ? (<><Spinner size="sm" color="light" /> Carregando...</>) : "Salvar"}
      </Button>
      
      </Modal.Footer>
      </Modal>
      
    }
    
    return (
      <>
      <CreateEst/>
      <CustomModel/>
      
      
      <BootstrapTable keyField='id' data={ Carros } columns={ columns }/>
      
      </>
      
      )
    }
    
    export default TabelaCarros
    
    
    