import React, { useEffect, useState, useCallback } from "react";
import Header from "../../Layout/header";
import {getUser}  from "../../../services/user.service";
import { useParams } from "react-router";
import { Link } from "react-router-dom";
import { Sign } from '../../../assets/styled';
import { setUsuarioAction } from '../../../store/usuario/usuario.action'
import { formatFactory } from '../../../utils/formatFactory'

import { useDispatch, useSelector } from 'react-redux';
import {
  Form, FormGroup, Input,
  Card, Col, CardBody,
  CardHeader,
  Button, CardFooter, Label, Alert, Spinner
} from 'reactstrap';
export default function Carro({ id }) {
  
  const [hasError, setHasError] = useState(false)
  const [HasCritical, setHasCriticalError] = useState(false)
  
  const error = useSelector(state => state.carro.error)
  const [messageError, setMessageError] = useState('')
  
  useEffect(() => {
    setHasCriticalError(error.length > 0)
    if(error.length > 0){
      setMessageError(error[0].error)
    }
  }, [error])
  
  const dispatch = useDispatch();
  let jsonReceveid = {}
  
  const closeError = () => setHasError(false);
  const [state, setState] = useState({
    tipo: ''
  })
  const [result, setResult] = useState({
    estacionamento: false,
    usuario: false
  })
  const isNotValid = () => {
    const inputs = Object.keys(state)
    const invalid = (label) => !Object.keys(state).includes(label) || state[label].length === 0
    
    return inputs.some(item => invalid(item))

  }
  const JsonCorrection = (label) => {
    var acceptables = ['tipo']
    acceptables.some((item) => {
      if(label != item){
        console.log('delete',label)
        delete jsonReceveid[label]
      } 
    })
    
    setState(jsonReceveid)
    if(!isNotValid()){
      dispatch(setUsuarioAction({...state},false))
    }
  }
  useEffect( () => {
    const selectUsuario = async () =>{
      await getUser().then((sucess)=>{
        jsonReceveid = sucess.data 
        switch(jsonReceveid.tipo){
          case 1:
            result['estacionamento']=true
         
          break
          case 0:
            result['usuario']=true
      
          break
        }
        setResult({...result},result)
        console.log(jsonReceveid)
        Object.keys(jsonReceveid).some(item => JsonCorrection(item))
      })
    }
    selectUsuario()
  }, []);
  const handleChange = (props) => {
    const { value, name } = props.target;

switch(name){
  case 'tipo':
    if(value == '1'){
      result['estacionamento']=true
      result['usuario']=false

    }else{
      result['usuario']=true
      result['estacionamento']=false

    }
 
 break
}
setResult({...result},result)
    
    state[name] = value
    setState({...state},state);


    setHasError(isNotValid())
    
    if(!isNotValid()){
      dispatch(setUsuarioAction({...state},false))
    }else{
      dispatch(setUsuarioAction({},true))
    } 
  }
  return (
    <Sign>
    <Col sm={12} md={10} lg={10}>
    <Card>
    <CardHeader tag="h4" className="text-center"></CardHeader>
    <CardBody>

    <center>É necessário efetuar login novamente para que seu perfil seja atualizado.</center>
    <Alert color="warning" isOpen={hasError} toggle={closeError}>
    <div><strong>OPS !!! </strong> Aconteceu um erro.</div>
    <small>Preencha todos os campos.</small>
    </Alert>
    <Alert color="danger" isOpen={HasCritical} toggle={closeError}>
    <div><strong>OPS !!! </strong> Aconteceu um erro.</div>
    <small>{messageError}</small>
    </Alert>
    <Form>
    
    
    <FormGroup check>
    <Label check>
    <Input type="radio" name="tipo" onChange={handleChange}  checked={result['usuario']} value="0"/>
    Eu preciso de um estacionamento
    </Label>
    <br/>
    <Label check>
    <Input type="radio" name="tipo" onChange={handleChange} checked={result['estacionamento']} value="1"/>
    Eu tenho um estacionamento
    </Label>
    </FormGroup>
    
    </Form>
    </CardBody>
    
    
    </Card>
    </Col>
    </Sign>
    
    );
  }
  