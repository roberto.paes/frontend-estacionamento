import React, { useEffect, useState, useCallback } from "react";
import Header from "../../Layout/header";
import {getEstacionamentoId}  from "../../../services/est.service";
import {setEstacionamentoAction}  from "../../../store/estacionamento/estacionamento.action";
import BootstrapTable from 'react-bootstrap-table-next';

import { useParams } from "react-router";
import { Link } from "react-router-dom";
import { Sign } from '../../../assets/styled';
import filterFactory, { customFilter } from 'react-bootstrap-table2-filter';
import moment from 'moment';
import Datetime from 'react-datetime';
import 'moment/locale/pt-br';
import Select from 'react-select'

import "react-datetime/css/react-datetime.css";
import { useDispatch, useSelector } from 'react-redux';
import {
    Form, FormGroup, Input,
    Card, Col, CardBody,
    CardHeader,
    Button, CardFooter, Label, Alert, Spinner
} from 'reactstrap';
import { Divider } from "@material-ui/core";
export default function Reserva({estacionamento,id}) {
    
    const estacionamentoSelector =  estacionamento.find(element => element.id == id);
    const [date,setDate] = useState({})
    
    const reservasDefault = estacionamentoSelector.reservas
    const [reservas,setReservas] = useState(reservasDefault)
    const [options,setOptions] = useState(false)
    
    const [total,setTotal] = useState(reservas.reduce(function(soma, item){
        return soma + parseInt(item.preco);
    }, 0.00))
    const precoCalc = () => {   
        setTotal(reservas.reduce(function(soma, item){
            return soma + parseInt(item.preco);
        }, 0.00))
        
    }
    useEffect(()=>{
        precoCalc()
    },[reservas])
    const reservaCount = reservas.length
    function dataFormatter(cell, row, rowIndex, formatExtraData) { 
        const date = moment(new Date(cell)).format('DD/MM/YYYY LT')
        return  (<> {date} </>)
    }
    function moneyFormat(cell, row, rowIndex, formatExtraData){
        const money = parseInt(cell).toLocaleString('pt-br',{style: 'currency', currency: 'BRL'})
        return ( 
            <>
            < div 
            style={{
                cursor: "pointer",
                display: "flex"
            }}>
            {money}
            </div>
            </>
            ); 
        }
        function statusFormater (cell, row, rowIndex, formatExtraData) { 
            
            const Status = cell ? 'Habilitado' : 'Inativo'
            return ( 
                <>
                < div 
                style={{
                    cursor: "pointer",
                    display: "flex"
                }}>
                
                {Status}
                </div>
                </>
                ); 
            }
            
            const columns = [{
                dataField: 'nome',
                text: 'Título',
                sort: true
            }, 
            {
                dataField: 'entrada',
                text: 'Entrada',
                formatter:dataFormatter,
                
                sort: true
            }
            , {
                dataField: 'saida',
                text: 'Saida',
                formatter:dataFormatter,
                sort: true
            }
            , {
                dataField: 'status',
                text: 'Status',
                formatter:statusFormater,
                sort: true
            }, {
                dataField: 'preco',
                text: 'Total',
                formatter:moneyFormat, 
                sort: true
            }
            
        ];
        const handleDateTimePicker = (momentReceived, prop) =>{
            if(momentReceived){
                date[prop] = momentReceived
            }else{
                date[prop.name] = prop.value
            }
            
            
            setDate({...date},date);
            
            setReservas(reservasDefault.filter((item)=>
            {
                if(!date['status']){
                    if(date['entradaInicial'] && date['entradaSaida'] && !date['saidaInicial'] && !date['saidaSaida']){
                        
                        return moment(item.entrada).isBetween(date['entradaInicial'],date['entradaSaida'],undefined,'[]')
                    }else if (date['saidaInicial'] && date['saidaSaida'] && !date['entradaInicial'] && !date['entradaSaida']){
                        return  moment(item.saida).isBetween(date['saidaInicial'],date['saidaSaida'],undefined,'[]') 
                    }else if (date['entradaInicial'] && date['entradaSaida'] && date['saidaInicial'] && date['saidaSaida']){
                        return moment(item.saida).isBetween(date['saidaInicial'],date['saidaSaida'],undefined,'[]') && moment(item.entrada).isBetween(date['entradaInicial'],date['entradaSaida'],undefined,'[]')
                    }
                    else{
                        setReservas(reservasDefault)
                    }
                }else{
                    if(date['entradaInicial'] && date['entradaSaida'] && !date['saidaInicial'] && !date['saidaSaida']){
                        
                        return moment(item.entrada).isBetween(date['entradaInicial'],date['entradaSaida'],undefined,'[]') && date['status'] && item.status === parseInt(date['status'])
                    }else if (date['saidaInicial'] && date['saidaSaida'] && !date['entradaInicial'] && !date['entradaSaida']){
                        return  moment(item.saida).isBetween(date['saidaInicial'],date['saidaSaida'],undefined,'[]') && date['status'] && item.status === parseInt(date['status'])
                    }else if (date['entradaInicial'] && date['entradaSaida'] && date['saidaInicial'] && date['saidaSaida']){
                        return moment(item.saida).isBetween(date['saidaInicial'],date['saidaSaida'],undefined,'[]') && moment(item.entrada).isBetween(date['entradaInicial'],date['entradaSaida'],undefined,'[]') && date['status'] && item.status === parseInt(date['status'])
                    }else{
                        return date['status'] && item.status === parseInt(date['status'])
                    }
                }
                
            }))
            console.log(date)
        }
        const clearFilter = (name) =>{
            console.log('cleaning..',name)
            
            date[name] = ''
            
            
            
            
            setDate({...date},date);
            if (!date['entradaInicial'] && !date['entradaSaida'] && !date['saidaInicial'] && !date['saidaSaida'] || !date['status']){
                setReservas(reservasDefault)
            }
            
        }
        
        
        const AdvancedOptions = () =>{
            if(options){
                setOptions(false)
            }else{
                setOptions(true)
            }
        }
        const statusArray = [{name:'status',value:'1',label:'Habilitado'},{name:'status',value:'0',label:'Inativo'}]
        
        return (
            <Sign>
            <Col sm={15} md={10} lg={15}>
            <Card>
            <CardBody>
            
            <Form>
            <strong>Estacionamento: {estacionamentoSelector.nome}</strong>
            <br/>
            <strong>Ganho total: {total.toLocaleString('pt-br',{style: 'currency', currency: 'BRL'})}</strong>
            <br/>
            <strong>Reservas: {reservaCount}</strong>
            <br/>
            <Button color="warning" onClick={AdvancedOptions}>
            Opções avançadas
            </Button>
            { options && (
                <>
                <FormGroup>
                <Label for="entrada">Entrada inicial:</Label> <Button onClick={() => clearFilter('entradaInicial')} close />
                <Datetime  
                timeFormat={false}
                value = {date['entradaInicial']}
                renderInput={(props) => {
                    return <input {...props} value={(date['entradaInicial']) ? props.value : ''} />
                }}
                onChange={moment => handleDateTimePicker(moment, 'entradaInicial')}
                inputProps={{ placeholder: "Data e hora de entrada"}}
                />
                
                
                <Label for="entrada">Entrada final:</Label><Button onClick={() => clearFilter('entradaSaida')} close />
                <Datetime
                timeFormat={false}
                
                value = {date['entradaSaida']}
                
                renderInput={(props) => {
                    return <input {...props} value={(date['entradaSaida']) ? props.value : ''} />
                }}
                onChange={moment => handleDateTimePicker(moment, 'entradaSaida')}
                inputProps={{ placeholder: "Data e hora de saida"}}
                />
                <Divider></Divider>
                <Label for="saida">Saida inicial:</Label><Button onClick={() => clearFilter('saidaInicial')} close />
                <Datetime
                timeFormat={false}
                
                value={date['saidaInicial']}
                renderInput={(props) => {
                    return <input {...props} value={(date['saidaInicial']) ? props.value : ''} />
                }}
                onChange={moment => handleDateTimePicker(moment, 'saidaInicial')}
                inputProps={{ placeholder: "Data e hora de saida"}}
                />
                <Label for="saida">Saida final:</Label><Button onClick={() => clearFilter('saidaSaida')} close />
                <Datetime
                timeFormat={false}
                
                value={date['saidaSaida']}
                renderInput={(props) => {
                    return <input {...props} value={(date['saidaSaida']) ? props.value : ''} />
                }}
                onChange={moment => handleDateTimePicker(moment, 'saidaSaida')}
                inputProps={{ placeholder: "Data e hora de saida"}}
                />
                </FormGroup>
                <FormGroup>
                <Label for="entrada">Status:</Label> <Button onClick={() => clearFilter('status')} close />
                
                <Select
                placeholder="Selecionar"
                options={statusArray}
                onChange={props => handleDateTimePicker(undefined, props)}
                value={statusArray.find((item)=> item.value === date['status']) || ''}
                />
                </FormGroup>
                </>
                ) }
                
                
                
                </Form > 
                </CardBody>
                
                <BootstrapTable keyField='id' data={ reservas } filter={ filterFactory() }  columns={ columns }/>
                
                </Card>
                </Col>
                
                </Sign>
                
                
                );
            }
            