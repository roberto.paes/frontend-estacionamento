
import { Link } from 'react-router-dom'
import BootstrapTable from 'react-bootstrap-table-next';
import React, { useEffect, useState } from 'react';
import EstModel from './model';
import EstDetalhes from './detalhes';
import store from './../../../store';
import {
  Form, FormGroup, Input,
  Card, Col, CardBody,
  CardHeader,
  Button, CardFooter,Label, Alert, Spinner
} from 'reactstrap';
import Modal from 'react-bootstrap/Modal'
import { finishEstacionamentoAction,  postEstacionamentoAction,refreshEstacionamentoAction,updateEstacionamentoAction,deleteEstacionamentoAction } from '../../../store/estacionamento/estacionamento.action'
import { useDispatch, useSelector } from 'react-redux';
import { useForkRef } from '@material-ui/core';
const TabelaReserva = () => {
  const dispatch = useDispatch();
  const [hasError, setHasError] = useState(false)
  
  
  const state = store.getState();
  
  const Estacionamentos = useSelector(state => state.estacionamento.estacionamentosUsuario)
  
  
  
  useEffect(()=>{
    dispatch(refreshEstacionamentoAction())
  },[])
  
  
  const [modal, setModal] = useState();
  
  
  const [item, setItem] = useState({});
  const closeError = () => setHasError(false);
  const handleCancel= () => setModal(false)
  const handleClose = () => {
    
    setModal(false)
    
  };
  
  useEffect(()=>{
    if(!modal){
      dispatch(refreshEstacionamentoAction())
    }
  },[modal])
  const handleShow = (type,row) => {   
    setModal(true)
    dispatch(finishEstacionamentoAction())
    
    setItem({type,row})
    
  }
  
  const TYPES = {
    ESTACIONAMENTO_LOADING:"ESTACIONAMENTO_LOADING",
    ESTACIONAMENTO_INSERT:"ESTACIONAMENTO_INSERT",
    ESTACIONAMENTO_REFRESH:"ESTACIONAMENTO_REFRESH",
    ESTACIONAMENTO_DETALHES:"ESTACIONAMENTO_DETALHES",
    ESTACIONAMENTO_UPDATE:"ESTACIONAMENTO_UPDATE",
    ESTACIONAMENTO_DEL:"ESTACIONAMENTO_DEL"
  }
  
  function rankFormatter(cell, row, rowIndex, formatExtraData) { 
    return ( 
      <>
      < div 
      style={{
        cursor: "pointer",
        display: "flex"
      }}>
      <Button color="info" style={{marginLeft:'10px'}} onClick={() => {
        handleShow(TYPES.ESTACIONAMENTO_DETALHES,row);
      }}>Detalhes</Button>
      <Button color="warning" style={{marginLeft:'10px'}} onClick={() => {
        handleShow(TYPES.ESTACIONAMENTO_UPDATE,row);
      }}>Editar</Button>
      <Button color="danger" style={{marginLeft:'10px'}} onClick={() => {
        handleShow(TYPES.ESTACIONAMENTO_DEL,row);
      }}>Deletar</Button>
      </div>
      </>
      ); }
      function reservaTotal(cell, row, rowIndex, formatExtraData) { 
        
        const estacionamentoSelector =  Estacionamentos.find(element => element.id == row.id);
        const reservasDefault = estacionamentoSelector.reservas
        const total = reservasDefault.filter((item)=> item.status == 1)
        
        return ( 
          <>
          < div 
          style={{
            cursor: "pointer",
            display: "flex"
          }}>
          {total.length}
          </div>
          </>
          ); }
          function ganhoTotal(cell, row, rowIndex, formatExtraData) { 
            
            const estacionamentoSelector =  Estacionamentos.find(element => element.id == row.id);
            const reservasDefault = estacionamentoSelector.reservas
            const total = reservasDefault.reduce(function(soma, item){
              return soma + parseInt(item.preco);
            }, 0.00)
            
            return ( 
              <>
              < div 
              style={{
                cursor: "pointer",
                display: "flex"
              }}>
              {total.toLocaleString('pt-br',{style: 'currency', currency: 'BRL'})}
              </div>
              </>
              ); }
              function statusFormater (cell, row, rowIndex, formatExtraData) { 
                
                const Status = cell ? 'Habilitado' : 'Inativo'
                return ( 
                  <>
                  < div 
                  style={{
                    cursor: "pointer",
                    display: "flex"
                  }}>
                  
                  {Status}
                  </div>
                  </>
                  ); 
                }
                
                
                const Title = () => {
                  switch(item.type){
                    case TYPES.ESTACIONAMENTO_DETALHES:
                    return  <Modal.Title>Detalhes</Modal.Title>
                    case TYPES.ESTACIONAMENTO_UPDATE:
                    return  <Modal.Title>Mudar título</Modal.Title>
                    case TYPES.ESTACIONAMENTO_DEL:
                    return  <Modal.Title>Deletar estacionamento</Modal.Title>
                    case TYPES.ESTACIONAMENTO_INSERT:
                    return  <Modal.Title>Cadastrar estacionamento</Modal.Title>
                  }
                  
                }
                
                const Body = () => {
                  
                  switch(item.type){
                    case TYPES.ESTACIONAMENTO_DETALHES:
                    state.estacionamento.isValid = false
                    
                    return  <Modal.Body><EstDetalhes estacionamento={Estacionamentos} id={item.row.id}/></Modal.Body>
                    case TYPES.ESTACIONAMENTO_DEL:
                    state.estacionamento.isValid = false
                    
                    return  <Modal.Body>Tem certeza que deseja excluir essa estacionamento? Essa ação não pode ser desfeita.</Modal.Body>
                    case TYPES.ESTACIONAMENTO_UPDATE:
                    return  <Modal.Body><EstModel id={item.row.id}/></Modal.Body>
                    case TYPES.ESTACIONAMENTO_INSERT:
                    return  <Modal.Body><EstModel/></Modal.Body>
                  }
                }
                
                const Salvar = async () =>{
                  
                  
                  switch(item.type){
                    
                    case TYPES.ESTACIONAMENTO_UPDATE:
                    await dispatch(updateEstacionamentoAction(item.row.id,state.estacionamento.estacionamentoCache))
                    //  dispatch(updateEstacionamentoAction(item.row.id,{teste:''})
                    break;
                    case TYPES.ESTACIONAMENTO_INSERT:  
                    
                    await dispatch(postEstacionamentoAction(state.estacionamento.estacionamentoCache))                    
                    break;
                    case TYPES.ESTACIONAMENTO_DEL:
                    await dispatch(deleteEstacionamentoAction(item.row.id))
                    break;
                  }
                  //     dispatch(finishEstacionamentoAction())
                  if(!state.estacionamento.error.length > 0){
                    setModal(false) 
                  }
                  //dispatch(refreshEstacionamentoAction())
                  
                  
                  
                  ///handleCancel()
                }
                const columns = [
                  {
                    dataField: 'nome',
                    text: 'Nome',
                    sort: true
                  }, {
                    dataField: 'logradouro',
                    text: 'Endereço',
                    sort: true
                  },
                  {    
                    text: 'Reservas',
                    dataField: 'reserv',
                    
                    formatter: reservaTotal,
                    sort: true
                  },
                  {    
                    text: 'Ganho total',
                    dataField: 'total',
                    formatter: ganhoTotal,
                    sort: true
                  },
                  { dataField: "edit", 
                  sort: false,
                  text: '',
                  dataField: 'edit',
                  formatter: rankFormatter,
                  headerAttrs: { width: 50 },
                  attrs: { width: 50} 
                }
                
              ];
              const CreateEst = () =>{
                return (<Button color="info" onClick={()=>handleShow(TYPES.ESTACIONAMENTO_INSERT,-1)} >Cadastrar estacionamento</Button>)
              }
              
              const CustomModel = () =>{
                
                const canDoIt = useSelector(state => state.estacionamento.isValid)
                const loading = useSelector(state => state.estacionamento.loading)
                return <Modal show={modal} onHide={handleClose} scrollable={true} size="lg"
                dialogClassName='custom-dialog'
                centered>
                <Modal.Header closeButton>
                <Title/>
                </Modal.Header>
                
                <Body/>
                <Modal.Footer>
                <Button color="danger" onClick={handleCancel}>
                Cancelar
                </Button>
                <Button color={canDoIt|| loading ? 'secondary' : 'success'} disabled={canDoIt} onClick={Salvar}>
                {loading ? (<><Spinner size="sm" color="light" /> Carregando...</>) : "Salvar"}
                </Button>
                
                </Modal.Footer>
                </Modal>
                
              }
              
              return (
                <>
                <CreateEst/>
                <CustomModel/>
                
                
                <BootstrapTable  keyField="id" data={ Estacionamentos } columns={ columns }/>
                
                </>
                
                )
              }
              
              export default TabelaReserva
              
              
              