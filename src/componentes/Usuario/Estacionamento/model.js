import React, { useEffect, useState, useCallback } from "react";
import Header from "../../Layout/header";
import {getEstacionamentoId}  from "../../../services/est.service";
import {setEstacionamentoAction,finishEstacionamentoAction}  from "../../../store/estacionamento/estacionamento.action";
import BootstrapTable from 'react-bootstrap-table-next';
import cellEditFactory from 'react-bootstrap-table2-editor';
import store from './../../../store';
import { useParams } from "react-router";
import { Link } from "react-router-dom";
import { Sign } from '../../../assets/styled';
import { estados } from '../../../utils/estados';
import { cidades } from '../../../utils/cidades';
import Select from 'react-select'
import cep from 'cep-promise'
import { useDispatch, useSelector } from 'react-redux';
import { formatFactory } from '../../../utils/formatFactory';
import { getCep } from '../../../services/google.service'
import { goToAnchor } from 'react-scrollable-anchor'

import {
  Form, FormGroup, Input,
  Card, Col, CardBody,
  CardHeader,
  Button, CardFooter, Label, Alert, Spinner
} from 'reactstrap';

export default function Model({ id }) {
  const stateRedux = store.getState();
  const [cepState, setCep] = useState(false)
  
  const [hasError, setHasError] = useState(false)
  const [formValidator, setFormValidator] = useState(
    {
      nome: {
        "status": false,
        "message": "Não é possível inserir palavras em vagas"
      },
      vagas: {
        "status": false,
        "message": "Não é possível inserir palavras em vagas"
      },
      cep: {
        "status": false,
        "message": "Não é possível inserir palavras em vagas"
      },
      logradouro:  {
        "status": false,
        "message": "Não é possível inserir palavras em vagas"
      },
      bairro: {
        "status": false,
        "message": "Não é possível inserir palavras em vagas"
      },
      estado: {
        "status": false,
        "message": "Não é possível inserir palavras em vagas"
      },
      cidade: {
        "status": false,
        "message": "Não é possível inserir palavras em vagas"
      },
      numero: {
        "status": false,
        "message": "Não é possível inserir palavras em vagas"
      }
    }
    )
    
    const [HasCritical, setHasCriticalError] = useState(false)
    const error = useSelector(state => state.estacionamento.error)
    const [messageError, setMessageError] = useState('')
    
    const [state, setState] = useState({
      nome: '',
      vagas: '',
      cep: '',
      logradouro: '',
      bairro: '',
      estado: '',
      cidade: '',
      numero: '',
      tarifa: [
        {
          "id":0,
          "tempo":15,
          "preco":0.00
        },
        {
          "id":1,
          "tempo":25,
          "preco":10.00
        },
        {
          "id":2,
          "tempo":30,
          "preco":15.00
        },
        {
          "id":3,
          "tempo":60,
          "preco":20.00
        },
        {
          "id":4,
          "tempo":"diaria",
          "preco":50.00
        }
      ]
    })
    const [estado,setEstado] = useState({})
    const remapEstados = estados.map((item)=>{
      return {tipo:'estado',label:item.Nome,value:item.Sigla,id:item.ID}
    })
    const remapCidades = cidades.map((item)=>{
      return {tipo:'cidade',label:item.Nome,value:item.Nome,estado:item.Estado}
    })
    useEffect(() => {
      const userCallBack = async () =>{
        setHasCriticalError(error.length > 0)
        if(error.length > 0){
          var cache = stateRedux.estacionamento.estacionamentoCache
          if(Object.keys(cache).length > 0){
            setMessageError(error[0].error)
            Object.keys(state).some((item) => {
              state[item] = cache[item] 
            })
            
            setState({...state},state)
            dispatch(setEstacionamentoAction({...state},false))     
            
            goToAnchor('message')

          }
        }
      }
      userCallBack()
    }, [error])
    useEffect(() => {
      const timeoutId = setTimeout(() => {
        setHasError(isNotValid())
      }, 1000);
      return () => clearTimeout(timeoutId);
    }, [state,formValidator]);
    const dispatch = useDispatch();
    let jsonReceveid = {}
    const closeError = () => setHasError(false);
    const closeErrorCritical = () => setHasCriticalError(false);  
    // const est = stateStore.estacionamento.estacionamentoCache
    const isNotValid = () => {
      const inputs = Object.keys(state)
      const empty = (label) => !Object.keys(state).includes(label) || state[label].length === 0 && label != 'complemento'
      var validation = []
      
      for(var item in formValidator){
        validation.push([formValidator[item]])
      }
      
      var checker = validation.some((item) => item[0]['status'] == true)
    console.log('status',checker)  
      if(checker){
        dispatch(setEstacionamentoAction({},true))
        return true
      }else{
        dispatch(setEstacionamentoAction({...state},false))
      }
      return inputs.some(item => empty(item))
      
    }
    const JsonCorrection = (label) => {
      if(jsonReceveid[label] == null || jsonReceveid[label] == undefined ){
        jsonReceveid[label] = ""
      }
      setEstado(remapEstados.find((t)=> t.value === jsonReceveid['estado']))
    }
    useEffect( () => {
      const selectEstacionamento = () =>{
        const callBackAsync = async () =>{
          await getEstacionamentoId(id).then((sucess)=>{  
            jsonReceveid = sucess.data 
            delete jsonReceveid['createdAt']
            delete jsonReceveid['usuarioId']
            delete jsonReceveid['updatedAt']
            delete jsonReceveid['id']    
            delete jsonReceveid['vaga_ocupada']         
            jsonReceveid['tarifa'] = sucess.data.tarifa.tarifa
            Object.keys(jsonReceveid).some(item => JsonCorrection(item))
            jsonReceveid['cep'] = formatFactory(jsonReceveid['cep'], 'xxxxx-xxx')
            setState(jsonReceveid)
            dispatch(setEstacionamentoAction({...jsonReceveid},false))
          })
        }
        callBackAsync()
      }
      if(id){
        
        if(!error.length > 0 ){
          selectEstacionamento()
        }
        
        
      }
      
      //  
      
      
    }, [])
    const handleValidator = (props) =>{
      const { value, name } = props.target;
      
      switch(name){
        case 'vagas':
        var isInteger = /^[0-9]+$/.test(value)
        console.log('vagas set filter', value,isInteger)
        setFormValidator({...formValidator,[name]:{status:!isInteger,message:`Não é possível inserir palavras em ${name}`}})
        break
        case 'bairro':
        var isString = /^([a-zA-Z\u00C0-\u00FF]\s*)+$/.test(value)
        setFormValidator({...formValidator,[name]:{status:!isString,message:`Não é possível inserir números em ${name}`}})
        break
        case 'nome':
        var isString = /^([a-zA-Z\u00C0-\u00FF]\s*)+$/.test(value)
        setFormValidator({...formValidator,[name]:{status:!isString,message:`Não é possível inserir números em ${name}`}})
        break
        case 'logradouro':
        var isString = /^([a-zA-Z\u00C0-\u00FF]\s*)+$/.test(value)
        setFormValidator({...formValidator,[name]:{status:!isString,message:`Não é possível inserir números em ${name}`}})
     
        break
        case 'numero':
        var isInteger = /^[0-9]+$/.test(value)
        setFormValidator({...formValidator,[name]:{status:!isInteger,message:`Não é possível inserir palavras em ${name}`}})
        break
        case 'cep':
        setCep(true)
        var iscepValid = /[0-9]{5}[\d]{3}/.test(value) || /[0-9]{5}-[\d]{3}/.test(value)
        if(iscepValid){
          cep(value,{ timeout: 5000, providers: ['brasilapi'] })
          .then((item) =>{
            state['estado'] = item.state
            state['cidade'] = item.city
            state['logradouro'] = item.street
            state['bairro'] = item.neighborhood
            setEstado(remapEstados.find((t)=> t.value === item.state))
            
            setState({...state},state)
            setCep(false)
            console.log('item',item)
          }).catch((item)=>{
            setCep(false)
            setFormValidator({...formValidator,[name]:{status:true,message:`Cep inválido. ${name}`}})
            
            console.log('error on give cpf')
          })
        }else{
          setCep(false)
        }
        setFormValidator({...formValidator,[name]:{status:!iscepValid,message:`Não é possível inserir palavras em ${name}`}})
        
        break
      }
      console.log('execute test validator',JSON.stringify(formValidator))
    }
    
    const refreshCep = async () =>{
      console.log('clicando')
      console.log(JSON.stringify(state))
      var logradouro = state['logradouro'], estado = state['estado'], cidade = state['cidade']
      if(logradouro && estado && cidade){
        logradouro = logradouro.replace(/\s/g,'+')
        await getCep(estado,cidade,logradouro).then((item)=>{
          state['cep'] = item.data[0].cep
          console.log('cep info',JSON.stringify(item.data))
          setState({...state},state)
          
        }).catch(()=>{
          setFormValidator({...formValidator,['cep']:{status:true,message:`Não foi possível indentificar um cep válido.`}})
          
        })
        
        
      }
      
    }
    const handleChange = (props) => {
      const { value, name } = props.target;
      handleValidator(props)

      state[name] = value
      
      setState({...state},state,state['cep'] = state['cep'].replace(/\.|\-/g, ''));
      
      if(!isNotValid()){
        //state['estado'] = estados.find(c => c.ID == state['estado']).Sigla
        dispatch(setEstacionamentoAction({...state},false))
      }else{
        dispatch(setEstacionamentoAction({},true))
      }
    }
    function moneyFormat(cell, row, rowIndex, formatExtraData){
      const money = parseInt(cell).toLocaleString('pt-br',{style: 'currency', currency: 'BRL'})
      return ( 
        <>
        < div 
        style={{
          cursor: "pointer",
          display: "flex"
        }}>
        {money}
        </div>
        </>
        ); 
      }
      function tempoFormat(cell, row, rowIndex, formatExtraData){
        
        if(rowIndex === 4){
          return <>diaria</>
        }else if(rowIndex == 3){
          return ( 
            <>
            < div 
            style={{
              cursor: "pointer",
              display: "flex"
            }}>
            60
            </div>
            </>
            ); 
          }else{
            return ( 
              <>
              < div 
              style={{
                cursor: "pointer",
                display: "flex"
              }}>
              {cell}
              </div>
              </>
              ); 
            }
          }
          const columns = [{
            dataField: 'tempo',
            formatter:tempoFormat,
            text: 'Minuto'
          }, {
            dataField: 'preco',
            formatter:moneyFormat,
            text: 'Preço'
          }];
          
          function afterSaveCell(oldValue, newValue, row, column) {
            
            state['tarifa'][row.id] = row
            //   if(!isNotValid()){
            dispatch(setEstacionamentoAction({...state},false))
            // }
          }
          
          
          const handleEstados = (props) =>{
            // console.log('chamge',props)
            state[props.tipo] = props.value
            switch(props.tipo){
              case 'estado':
              setEstado(props)
              break
            }
            setState({...state},state);
            if(!isNotValid()){
              dispatch(setEstacionamentoAction({...state},false))
            }
            // console.log('state',state)
            //   console.log('props',props)
          }
          return (
            <Sign>
            <Col sm={12} md={10} lg={10}>
            <Card>
            <CardHeader tag="h4" className="text-center"/>
            <CardBody>
            <div id="message">
            <Alert color="danger" isOpen={hasError} toggle={closeError}>
            <div><strong>OPS !!! </strong> Aconteceu um erro.</div>
            <small>Preencha todos os campos.</small>
            </Alert>
            <Alert color="danger" isOpen={HasCritical} toggle={closeErrorCritical}>
            <div><strong>OPS !!! </strong> Aconteceu um erro.</div>
            <small>{messageError}</small>
            </Alert>
            </div>
            <Form>
            <FormGroup>
            <Alert color="warning" isOpen={formValidator['nome']['status'] || false}>
            <small>{formValidator['nome']['message']}</small>
            </Alert>
            <Label for="nome">Nome:</Label>
            
            <Input
            placeholder="Nome"
            type="text"
            value={state.nome || ''}
            onChange={handleChange}
            name="nome"
            id="nome"
            />
            </FormGroup>
            <FormGroup>
            <Alert color="warning" isOpen={formValidator['vagas']['status'] || false}>
            <small>{formValidator['vagas']['message']}</small>
            </Alert>
            <Label for="vagas">Vagas:</Label>
            
            <Input
            placeholder="Vagas"
            type="text"
            value={state.vagas || ''}
            onChange={handleChange}
            name="vagas"
            id="vagas"
            />
            
            </FormGroup>
            <FormGroup>
            <Alert color="warning" isOpen={formValidator['cep']['status'] || false}>
            <small>{formValidator['cep']['message']}</small>
            </Alert>
            <Label for="cep">Cep:</Label>
            
            <Input
            placeholder="Cep"
            type="text"
            value={state.cep || ''}
            onChange={handleChange}
            name="cep"
            id="cep"
            />
            </FormGroup>
            <Alert color="warning" isOpen={cepState}>
            <small>Carregando informações referentes ao cep</small>
            </Alert>
            <FormGroup>
            <Label for="estado">Estado:</Label>
            <Select
            placeholder="Selecionar"
            options={remapEstados}
            onChange={handleEstados}
            value={remapEstados.filter(option => option.value === state['estado'])}
            />
            </FormGroup>
            
            <FormGroup>
            
            
            <Label for="cidade">Cidade:</Label>
            
            <Select
            placeholder="Selecionar"
            options={remapCidades.filter((item)=> item.estado === estado.id)}
            onChange={handleEstados}
            value={remapCidades.filter(option => option.value === state['cidade'])}
            /> 
            
            </FormGroup>
            
            <FormGroup>
            
            <FormGroup>
            <Alert color="warning" isOpen={formValidator['logradouro']['status'] || false}>
            <small>{formValidator['logradouro']['message']}</small>
            </Alert>
            <Label for="logradouro">Logradouro:</Label>
            
            <Input
            
            placeholder="Logradouro"
            type="text"
            value={state.logradouro || ''}
            onChange={handleChange}
            name="logradouro"
            id="logradouro"
            />
            </FormGroup>
            <FormGroup>
            <Alert color="warning" isOpen={formValidator['bairro']['status'] || false}>
            <small>{formValidator['bairro']['message']}</small>
            </Alert>
            <Label for="bairro">Bairro:</Label>
            
            <Input
            placeholder="Bairro"
            type="text"
            value={state.bairro || ''}
            onChange={handleChange}
            name="bairro"
            id="bairro"
            />
            </FormGroup>
            <FormGroup>
            
            <Button color="warning" onClick={()=>refreshCep()}>
            Atualizar meu cep
            </Button>
            
            </FormGroup>
            
            <FormGroup>
            <Alert color="warning" isOpen={formValidator['numero']['status'] || false}>
            <small>{formValidator['numero']['message']}</small>
            </Alert>
            <Label for="numero">Número:</Label>
            
            <Input
            placeholder="Número"
            type="text"
            value={state.numero || ''}
            onChange={handleChange}
            name="numero"
            id="numero"
            />
            </FormGroup>
            <FormGroup>
            <Label for="numero">Complemento(opcional):</Label>
            
            <Input
            placeholder="Complemento"
            type="text"
            value={state.complemento || ''}
            onChange={handleChange}
            name="complemento"
            id="complemento"
            />
            </FormGroup>
            
            
            
            <BootstrapTable 
            keyField="id"
            data={ state['tarifa'] }
            columns={ columns }
            cellEdit={ cellEditFactory({
              mode: 'click',
              blurToSave: true,
              afterSaveCell
            }) }
            />
            </FormGroup>
            
            </Form>
            </CardBody>
            
            
            </Card>
            </Col>
            </Sign>
            
            );
          }
          