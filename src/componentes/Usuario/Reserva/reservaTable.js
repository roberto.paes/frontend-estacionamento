
import { Link } from 'react-router-dom'
import BootstrapTable from 'react-bootstrap-table-next';
import React, { useEffect, useState } from 'react';
import ReservaEdit from './model';

import {
  Form, FormGroup, Input,
  Card, Col, CardBody,
  CardHeader,
  Button, CardFooter,Label, Alert, Spinner
} from 'reactstrap';
import Modal from 'react-bootstrap/Modal'
import { finishReservaAction, refreshReservaAction,updateReservaAction } from '../../../store/reserva/reserva.action'
import { useDispatch, useSelector } from 'react-redux';
import moment from 'moment';
import store from './../../../store';

const TabelaReserva = () => {
  const dispatch = useDispatch();
  const state = store.getState();
  const [modal, setModal] = useState(false);
  
  const Reservas = useSelector(state => state.reserva.reservas)
  useEffect(()=>{
    dispatch(refreshReservaAction())
  },[])
  const [item, setItem] = useState({});
  const handleClose = () => setModal(false);
  const handleCancel= () => setModal(false)
  
  const handleShow = (type,row) => {   
    setModal(true)
    dispatch(finishReservaAction())
    
    setItem({type,row})
    // console.log('row',row)
  }
  useEffect(()=>{
    if(!modal){
      dispatch(refreshReservaAction())
    }
  },[modal])
  const TYPES = {
    RESERVA_LOADING:"RESERVA_LOADING",
    RESERVA_REFRESH:"RESERVA_REFRESH",
    RESERVA_UPDATE:"RESERVA_UPDATE",
    RESERVA_CANCEL:"RESERVA_CANCEL"
  }
  function dataFormatter(cell, row, rowIndex, formatExtraData) { 
    const date = moment(new Date(cell)).format('DD/MM/YYYY LT')
    return  (<> {date} </>)
  }
  function rankFormatter(cell, row, rowIndex, formatExtraData) { 
    return ( 
      <>
      < div 
      style={{
        cursor: "pointer",
        display: "flex"
      }}>
      <Button color="warning" style={{marginLeft:'10px'}} onClick={() => {
        handleShow(TYPES.RESERVA_UPDATE,row);
      }}>Mudar título</Button>
      <Button color="danger" style={{marginLeft:'10px'}} disabled={row.status ? false : true} onClick={() => {
        handleShow(TYPES.RESERVA_CANCEL,row);
      }}>Cancelar</Button>
      </div>
      </>
      ); }
      function moneyFormat(cell, row, rowIndex, formatExtraData){
        const money = parseInt(cell).toLocaleString('pt-br',{style: 'currency', currency: 'BRL'})
        return ( 
          <>
          < div 
          style={{
            cursor: "pointer",
            display: "flex"
          }}>
          {money}
          </div>
          </>
          ); 
        }
        
        function statusFormater (cell, row, rowIndex, formatExtraData) { 
          
          const Status = cell ? 'Habilitado' : 'Inativo'
          return ( 
            <>
            < div 
            style={{
              cursor: "pointer",
              display: "flex"
            }}>
            
            {Status}
            </div>
            </>
            ); 
          }
          
          const columns = [{
            dataField: 'nome',
            text: 'Nome',
            sort: true
          }, {
            dataField: 'entrada',
            text: 'Entrada',
            formatter: dataFormatter,
            sort: true
          }, {
            dataField: 'saida',
            formatter: dataFormatter,
            text: 'Saida',
            sort: true
          },
          
          {
            dataField: 'status',
            formatter: statusFormater,
            text: 'Status',
            sort: true
          }
          , {
            dataField: 'preco',
            text: 'Total pago',
            formatter:moneyFormat,
            sort: true
          },
          { dataField: "edit", 
          text: "",
          sort: false,
          formatter: rankFormatter,
          headerAttrs: { width: 50 },
          attrs: { width: 50} 
        }
      ];
      const Title = () => {
        switch(item.type){
          case TYPES.RESERVA_UPDATE:
          return  <Modal.Title>Mudar título</Modal.Title>
          case TYPES.RESERVA_CANCEL:
          state.reserva.isValid = false
          
          return  <Modal.Title>Cancelar reserva</Modal.Title>
        }
        
      }
      const Body = () => {
        
        switch(item.type){
          case TYPES.RESERVA_CANCEL:
          return  <Modal.Body>Tem certeza que deseja cancelar essa reserva? Essa ação não pode ser desfeita.</Modal.Body>
          case TYPES.RESERVA_UPDATE:
          return  <Modal.Body><ReservaEdit id={item.row.id}/></Modal.Body>
        }
      }
      
      const Salvar = async () =>{
        
        switch(item.type){
          case TYPES.RESERVA_UPDATE:
          await   dispatch(updateReservaAction(item.row.id,state.reserva.reservaCache))
          break;
          case TYPES.RESERVA_CANCEL:
          await  dispatch(updateReservaAction(item.row.id,{status:0}))
          break;
        }
        //  dispatch(refreshReservaAction())
        if(!state.reserva.error.length > 0){
          setModal(false)
        }
        
        
      }
      
      const CustomModel = () =>{
        const canDoIt = useSelector(state => state.reserva.isValid)
        
        return <Modal show={modal} onHide={handleClose} scrollable={true} size="lg"
        dialogClassName='custom-dialog'
        centered>
        <Modal.Header closeButton>
        <Title/>
        </Modal.Header>
        
        <Body/>
        <Modal.Footer>
        <Button color="danger" onClick={handleCancel}>
        Cancelar
        </Button>
        <Button color={canDoIt|| state.reserva.loading ? 'secondary' : 'success'} disabled={canDoIt} onClick={Salvar}>
        {state.carro.loading ? (<><Spinner size="sm" color="light" /> Carregando...</>) : "Salvar"}
        </Button>
        
        </Modal.Footer>
        </Modal>
        
      }
      
      
      return (
        <>
        <CustomModel/>
        <BootstrapTable keyField='id' data={ Reservas } columns={ columns }/>
        
        </>
        
        )
      }
      
      export default TabelaReserva
      
      
      