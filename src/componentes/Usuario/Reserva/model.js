import React, { useEffect, useState, useCallback } from "react";
import Header from "../../Layout/header";
import {getReservaId}  from "../../../services/res.service";
import { useParams } from "react-router";
import { Link } from "react-router-dom";
import { Sign } from '../../../assets/styled';
import { setReservaAction } from '../../../store/reserva/reserva.action'
import { formatFactory } from '../../../utils/formatFactory'
import store from './../../../store';

import { useDispatch, useSelector } from 'react-redux';
import {
  Form, FormGroup, Input,
  Card, Col, CardBody,
  CardHeader,
  Button, CardFooter, Label, Alert, Spinner
} from 'reactstrap';
export default function Reserva({ id }) {
  const dispatch = useDispatch();
  
  let jsonReceveid = {}
  const [hasError, setHasError] = useState(false)
  const [HasCritical, setHasCriticalError] = useState(false)
  const error = useSelector(state => state.reserva.error)
  const [messageError, setMessageError] = useState('')
  const storeState = store.getState();
  const [formValidator, setFormValidator] = useState(
    {
      nome: {
        "status": false,
        "message": "Título inválido"
      }
      
    }
    )
    const [state, setState] = useState({nome:''});
    useEffect(() => {
      const userCallBack = async () =>{
        setHasCriticalError(error.length > 0)
        if(error.length > 0){
          var cache = storeState.reserva.reservaCache
          if(Object.keys(cache).length > 0){
            setMessageError(error[0].error)
            Object.keys(state).some((item) => {
              state[item] = cache[item] 
              setState({...state},state);
              dispatch(setReservaAction({...state},false))
            })
            
          }
        }
      }
      userCallBack()
    }, [error])
    const closeError = () => setHasError(false);
    
    const isNotValid = () => {
      const inputs = Object.keys(state)
      const invalid = (label) => !Object.keys(state).includes(label) || state[label].length === 0
      var validation = []
      for(var item in formValidator){
        validation.push([formValidator[item]])
      }
      
      var checker = validation.some((item) => item[0]['status'] == true)
      if(checker){
        dispatch(setReservaAction({},true))
        return true
      }else{
        dispatch(setReservaAction({...state},false))
      }
      return inputs.some(item => invalid(item))
    }
    const JsonCorrection = (label) => {
      
      var acceptables = ['status','nome']
      
      if(!acceptables.includes(label)){
        console.log('delete',label)
        delete jsonReceveid[label]
      } 
      
    }
    useEffect(() => {
      const timeoutId = setTimeout(() => {
        setHasError(isNotValid())
        console.log('set error')
      }, 1000);
      return () => clearTimeout(timeoutId);
    }, [state,formValidator]);
    useEffect( () => {
      const selectReserva = async () =>{
        await getReservaId(id).then((sucess)=>{
          jsonReceveid = sucess.data 
          Object.keys(jsonReceveid).some((item) => JsonCorrection(item))
          setState(jsonReceveid)
          dispatch(setReservaAction({...jsonReceveid},false))
          
        })
      }
      if(id){
        if(!error.length > 0 ){
          selectReserva()
        }
      }
    }, []);
    const handleValidator = (props) =>{
      const { value, name } = props.target;
      switch(name){
        case 'nome':
        var isString = /^([a-zA-Z0-9\u00C0-\u00FF]\s*)+$/.test(value)
        setFormValidator({...formValidator,[name]:{status:!isString,message:`Nome inválido. Apenas letras e números são aceitos.`}})
        console.log(isString)
        break
        
        
      }
    }
    const handleChange = (props) => {
      const { value, name } = props.target;
      state[name] = value
      setState({...state},state);
      setHasError(isNotValid())
      handleValidator(props)
      
      if(!isNotValid()){
        dispatch(setReservaAction({...state},false))
      }else{
        dispatch(setReservaAction({},true))
      } 
    }
    
    return (
      <Sign>
      <Col sm={12} md={10} lg={10}>
      <Card>
      <CardHeader tag="h4" className="text-center"></CardHeader>
      <CardBody>
      <Alert color="danger" isOpen={hasError} toggle={closeError}>
      <div><strong>OPS !!! </strong> Aconteceu um erro.</div>
      <small>Preencha todos os campos.</small>
      </Alert>
      <Alert color="danger" isOpen={HasCritical} toggle={closeError}>
      <div><strong>OPS !!! </strong> Aconteceu um erro.</div>
      <small>{messageError}</small>
      </Alert>
      <Form>
      
      <FormGroup>
      <Alert color="warning" isOpen={formValidator['nome']['status'] || false}>
      <small>{formValidator['nome']['message']}</small>
      </Alert>
      <Input
      placeholder="Título"
      type="text"
      value={state.nome || ''}
      onChange={handleChange}
      name="nome"
      id="nome"
      />
      </FormGroup>
      </Form>
      </CardBody>
      
      
      </Card>
      </Col>
      </Sign>
      
      );
    }
    