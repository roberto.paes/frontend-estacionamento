import React, { useEffect, useState} from "react";
import { useParams } from "react-router";
import { Sign } from '../../assets/styled';
import { useDispatch, useSelector } from 'react-redux';
import { Link } from 'react-router-dom';


import moment from 'moment';
import BootstrapTable from 'react-bootstrap-table-next';
import { getUser as getUserAuth } from './../../config/auth'
import Datetime from 'react-datetime';
import 'moment/locale/pt-br';
import "react-datetime/css/react-datetime.css";
import tarifaUtils from "../../utils/tarifa";
import {
  Form, FormGroup, Input,
  Card, Col, CardBody,
  CardHeader,
  Button, CardFooter,Label, Alert, Spinner
} from 'reactstrap';

import { postReservaAction,finishReservaAction } from '../../store/reserva/reserva.action'
import {getEstacionamentoAction} from '../../store/estacionamento/estacionamento.action'
import { getUser } from '../../services/user.service'
import { useHistory } from "react-router-dom";
import store from './../../store';
import { useLocation } from "react-router-dom";
import { goToAnchor } from 'react-scrollable-anchor'
export default function Formulario() {
  
  const { id } = useParams();
  const dispatch = useDispatch();
  const [preco, setPreco] = useState(0);
  const state = store.getState();
  const [user, setUser] = useState([]);
  const [warningMessageCar, setWarningMessageCar] = useState({active:true,message:'Selecione um carro'})
  const [warningMessageDate, setWarningMessageDate] = useState({active:true,message:'Entrada e saida devem ser diferentes.'})
  
  const location = useLocation();
  let history = useHistory();

  const [hasError, setHasError] = useState(false)
  const registered = useSelector(state => state.reserva.registred)
  const [success, showSuccess] = useState(false)
  const estacionamento = useSelector(state => state.estacionamento.estacionamento)
  const loading = useSelector(state => state.reserva.loading)
  const error = useSelector(state => state.reserva.error)
  const [messageError, setMessageError] = useState('')
  
  const [json, setJson] = useState({nome:'',saida:'',entrada:'',carroId:''});
  const closeError = () => setHasError(false);
  
  const [tarifa,setTarifa] = useState([
    {
      "id":0,
      "tempo":0,
      "preco":0.00
    },
    {
      "id":1,
      "tempo":0,
      "preco":0
    },
    {
      "id":2,
      "tempo":0,
      "preco":0
    },
    {
      "id":3,
      "tempo":0,
      "preco":0
    },
    {
      "id":4,
      "tempo":"diaria",
      "preco":0.00
    }
  ])
  useEffect(async () => {
    if(estacionamento.tarifa){
      setTarifa(estacionamento.tarifa.tarifa)
    }
  },[estacionamento])
  useEffect(() => {
    setHasError(error.length > 0)
    if(error.length > 0){
      setMessageError(error[0].error)
      console.log('error detected',error[0].error)
      goToAnchor('message')
      
    }
  }, [error])
  const handleDateTimePicker = (moment, name) =>{
    
    json[name] = moment
    
    checkDateValidate() 
    //moment.format('MM/DD/YYYY HH:mm:ss');
    setJson({...json},json);
    console.log('teste',JSON.stringify(json))
    setPreco(tarifaUtils.InitCalc(estacionamento.tarifa.tarifa,json.entrada,json.saida))
    
  } 
  const checkDateValidate = () =>{
    
    if(json['saida'] === json['entrada']){
      setWarningMessageDate({active:true,message:'Entrada e saida devem ser diferentes.'})
    }else{
      setWarningMessageDate({active:false})
    }
  }
  
  const handleSubmit = (event) => {
    dispatch(postReservaAction({estacionamentoId:id,...json}))
  }
  const isNotValid = () => {
    const inputs = Object.keys(json)
    if(!isLogged()){
      return true
    }
    console.log('carroid',json['carroId'] )
    if(warningMessageDate.active){
      return true
    }
    if(json['carroId'] == -1){
      //  setWarningMessage({active:true,message:'Selecione um carro.'})
      return true
    }
    const invalid = (label) => !Object.keys(json).includes(label) || json[label].length === 0
    return inputs.some(item => invalid(item))
  }
  const handleChange = (props) => {
    const { value, name } = props.target
    json[name] = value
    setJson({...json},json);
    if(json['carroId'] == -1){
      setWarningMessageCar({active:true,message:'Selecione um carro.'})
    }else{
      setWarningMessageCar({active:false,message:''})
      
    }
    console.log('test',{...json})
    
  }
  
  useEffect(() => {
    const userCallBack = async () =>{
      console.log('usuario state',isLogged())
      if(isLogged()){
        var res = await getUser()
        
        setUser(res.data)
        json['nome'] = 'Reserva de ' + res.data.nome
        if(location.state && location.state.entrada && location.state.saida){
          json['entrada'] = new Date(location.state.entrada)
          json['saida'] = new Date(location.state.saida)
          console.log('state reserva',location.state)
          
        }
        setJson(json);
        checkDateValidate()
        
      }
    }
    dispatch(finishReservaAction())
    dispatch(getEstacionamentoAction(id))
    userCallBack()
  }, []);
  var yesterday = moment().subtract( 1, 'day' );
  var valid = function( current ){
    return current.isAfter( yesterday );
  };
  
  const isLogged = () =>{
    if(Object.keys(getUserAuth()).length > 1){
      return true
    }else{
      return false
    }
  }
  useEffect(() => {
    if (registered) {
      showSuccess(true)
      setJson({})
      goToAnchor('message')
    }
  }, [registered])
  function moneyFormat(cell, row, rowIndex, formatExtraData){
    const money = parseInt(cell).toLocaleString('pt-br',{style: 'currency', currency: 'BRL'})
    if(rowIndex === 4){
      const result =  cell ? money : 'Esse estacionamento não cobra diária.'
      return ( 
        <>
        < div 
        style={{
          cursor: "pointer",
          display: "flex"
        }}>
        {result} 
        </div>
        </>
        ); 
      }else{
        return ( 
          <>
          < div 
          style={{
            cursor: "pointer",
            display: "flex"
          }}>
          {money}
          </div>
          </>
          ); 
        }
      }
      function tempoFormat(cell, row, rowIndex, formatExtraData){
        
        if(rowIndex === 4){
          return <>diaria</>
        }else if(rowIndex == 3){
          return <>60</>
        }else{
          return ( 
            <>
            < div 
            style={{
              cursor: "pointer",
              display: "flex"
            }}>
            {cell}
            </div>
            </>
            ); 
          }
        }
        const columnTarifa = [{
          dataField: 'tempo',
          formatter:tempoFormat,
          text: 'Minuto'
        }, {
          dataField: 'preco',
          formatter:moneyFormat,
          text: 'Preço'
        }];
        const goToPreviousPath = () => {
          history.goBack()
        }
        const ButtonSubmit =()=>{
          if(isLogged()){
            return <Button color={isNotValid() || loading ? 'secondary' : 'primary'} disabled={isNotValid()} size="sm" onClick={handleSubmit}>
            {loading ? (<><Spinner size="sm" color="light" /> Carregando...</>) : "Efetuar reserva" }
            </Button>
          }else{
            return <Button color="primary"  size="sm" onClick={()=>history.push({
              pathname: '/login',
              state: { redirect: `/reserva/${id}`,entrada:new Date(json['entrada']),saida:new Date(json['saida'])}
              
            })}>
            {loading ? (<><Spinner size="sm" color="light" /> Carregando...</>) : "Faça login" }
            </Button>
          }
          
        }
        return (
          <Sign>
          <Col sm={12} md={4} lg={5}>
          <div id="message">
          <Alert color="success" isOpen={success} toggle={() => showSuccess(!success)}>
          <div><strong>Reserva </strong> efetuada com sucesso.</div>
          </Alert>
          <Alert color="danger" isOpen={hasError} toggle={closeError}>
          <div><strong>OPS !!! </strong> Aconteceu um erro.</div>
          <small>{messageError}</small>
          </Alert>
          </div>
          <Card>
          <CardHeader tag="h4" className="text-center">Reserva em {estacionamento.nome}</CardHeader>
          { !isLogged() && (
            <center>
            <>  
            É necessário estar logado para efetuar uma reserva.
            </>
            </center>
            ) }
            <CardBody>
            <Form>
            <center><Label for="endereco">Endereço: {estacionamento.logradouro}, {estacionamento.bairro} - {estacionamento.estado}</Label></center>

            { isLogged() && (
              <>
              <FormGroup>
              <Label for="nome">Título(opcional)</Label>
              <Input disabled={loading} type="text" name="nome" onChange={handleChange} value={json.nome || ''}/>
              </FormGroup>
              <FormGroup>
              <Alert color="warning" isOpen={warningMessageCar['active']} toggle={closeError}>
              <small>{warningMessageCar['message']}</small>
              </Alert>


              <Label for="carroId">Selecione o seu carro:</Label>
              
              <Input type="select" name="carroId" onChange={handleChange} >
              <option value="-1">Selecione um carro</option>
              {user.carros && user.carros.map((carro) => (
                <option key={carro.id} value={carro.id}>Placa:{carro.placa} - Marca:{carro.marca}/Modelo:{carro.modelo}</option>
                ))}
                </Input>
                </FormGroup>
                </>
                ) }
                <FormGroup>
                <Label htmlFor="nome">
                <strong>Entrada</strong>
                </Label>
                <Alert color="warning" isOpen={warningMessageDate['active']} toggle={closeError}>
                <small>{warningMessageDate['message']}</small>
                </Alert>
                <Datetime
                value={json['entrada']}
                renderInput={(props) => {
                  return <input {...props} value={(json['entrada']) ? props.value : ''} />
                }}
                onChange={moment => handleDateTimePicker(moment, 'entrada')}
                isValidDate={ valid }
                
                inputProps={{ placeholder: "Data e hora de entrada"}}
                />
                </FormGroup>
                <FormGroup>
                <Label htmlFor="nome">
                <strong>Saida</strong>
                </Label>
                <Datetime
                
                value={json['saida']}
                renderInput={(props) => {
                  return <input {...props} value={(json['saida']) ? props.value : ''} />
                }}
                onChange={moment => handleDateTimePicker(moment, 'saida')}
                isValidDate={ valid }
                
                inputProps={{ placeholder: "Data e hora de saida"}}
                />
                </FormGroup>
                <FormGroup>
                <center>
                <Label htmlFor="Tabela de preços" >
                <strong>Tabela de preços</strong>
                </Label>
                <BootstrapTable 
                keyField="id"
                data={ tarifa  }
                columns={ columnTarifa }
                />
                
                
                </center>
                
                </FormGroup>
                <center>
                
                <ButtonSubmit/>
                <br/>
                <br/>
                <Button color='primary' onClick={goToPreviousPath} size="sm"  >
                Voltar
                </Button>
                
                </center>    
                <br/>
                </Form >
                <center>
                <strong>Preço: {preco.toLocaleString('pt-BR',{ style: 'currency', currency: 'BRL' })}</strong> 
                </center>
                </CardBody>
                <CardFooter className="text-muted">
                Não tem Cadastro? <Link to="/cadastro">Cadastre-se</Link>
                </CardFooter>
                
                </Card>
                </Col>
                </Sign>
                );
              }
              