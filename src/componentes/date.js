
import { Link } from 'react-router-dom'
const Time = ({date}) => {
    const Clock = ({nivel,...props}) => {
        function getTimeSpan() {
     
            var newDate = new Date(date)
         
            return {
                day: ("0" + (newDate.getUTCDate())).slice(-2),
                month: ("0" + (newDate.getUTCMonth() + 1)).slice(-2),
                year: ("0" + (newDate.getFullYear() )).slice(-2),
        
                hour: ("0" + (newDate.getUTCHours())).slice(-2),
                minute: ("0" + (newDate.getUTCMinutes() )).slice(-2),
                second: ("0" + (newDate.getUTCSeconds() )).slice(-2)
            }
        }
   

    return <div>{getTimeSpan().hour}:{getTimeSpan().minute}:{getTimeSpan().second}-{getTimeSpan().day}/{getTimeSpan().month}/{getTimeSpan().year}</div>
       }


    return (
        <div>
   <Clock/>
        </div>
                                                
    )
}

export default Time


