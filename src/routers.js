import React,{ useState , useEffect } from 'react';
import { BrowserRouter as Router,Switch, Route, Redirect } from "react-router-dom"
import { getUser } from './config/auth'
import { logoutAction } from './store/auth/auth.action'
import store from './store'
import history from './config/history';
import http from './config/http'
import {removeToken,removeUser}  from './config/auth';

//components basic
import Layout from './componentes/Layout'
import Footer from './componentes/Layout/footer'
import Header from './componentes/Layout/header'
// // views

// import Sobre from './../views/sobre'
import Login from './views/auth/login'
import Register from './views/auth/registrar'

import CarroTable from "./componentes/Usuario/Carro/model";
import UsuarioReserva from './views/Usuarios/reservas'
import Carros from './views/Usuarios/carros'
import Inicio from './views/inicio'

import Home from './views/Usuarios/home'
import Error404 from './views/erros404'

import estacionamentUsuario from './views/Usuarios/estacionamentos'

// import Reserva from '../views/reservas'
import Estacionamento from './views/estacionamento'

// import erros404 from '../views/erros404'
import EstacionamentoReserva from './componentes/Estacionamento/reserva'
import { useDispatch, useSelector } from 'react-redux';

const Routers = () => {
  
  const isLogged = useSelector(state => state.auth.isLogged)
  const usuario = useSelector(state => state.auth.usuario)
  const PrivateRoute = ({nivel,...props}) => {
    
    
    const isLoggedIn = getUser();
    console.log('llogged as ', isLoggedIn)
    //console.log('type',tipo)
    var niv = nivel || 0
    return (isLoggedIn?.tipo >= niv) ? <Route {...props} /> :   <Redirect 
    to={{
      pathname: '/erro404',
      state: 'test' 
    }} 
    />;
    
  }
  
  
  
  
  return(
    
    <Router history={history}>
    <Layout>   
    
    <Switch>
    
    
    
    {/* <Route exact path='/sobre' component={Sobre}></Route>
    
    
    <Route exact path='/cadastro' component={Cadastro}></Route>
    <PrivateRoute path="/reservas" component={Reserva}/>
    // <PrivateRoute path="/home" component={Home}/>
    // <PrivateRoute path="/carros" component={Carros}/>
    <PrivateRoute path="/carro/cadastro" component={CadCarro}/>
    <PrivateRoute path="/carro/edit/:id" component={CarroEdit}/>
    <PrivateRoute path="/carro/delete/:id" component={CarroDelete.DeleteCarro}/> 
    <PrivateRoute path="/reserva/:id" component={CadReserva}/> 
    <PrivateRoute nivel={1} path="/estacionamentos"/> 
    <PrivateRoute path="/logout" render={()=> {
      removeToken() 
      removeUser()
      setLoginState(false) 
    }}/>
    
    
  // <Route exact path='/estacionamento/:estado/:bairro' component={estacionamento}></Route> */}
  <Route exact path='/' component={Inicio}></Route>
  <PrivateRoute path="/carro/cadastro" component={CarroTable}/>
  
  <PrivateRoute path="/carros" component={Carros}/>
  <PrivateRoute path="/home" component={Home}/>
  <PrivateRoute nivel={1} path="/estacionamentos" component={estacionamentUsuario}/>
  <PrivateRoute path="/reservas" component={UsuarioReserva}/> 
  <Route path="/reserva/:id" component={EstacionamentoReserva}/> 
  <Route exact path='/login' component={Login} />
  <Route exact path='/cadastro' component={Register} />
  <Route exact path='/estacionamento/finder' component={Estacionamento}></Route>

  <Route exact path='/estacionamento/:estado/:cidade?/:bairro?' component={Estacionamento}></Route>
  <Route exact path='/erro404' component={Error404}></Route>
  <Redirect from="*" to="/erro404" />
  
  </Switch>
  </Layout>
  
  </Router>
  
  
  )
}

export default Routers