//import as libs
import { applyMiddleware, combineReducers, createStore } from "redux";
import { composeWithDevTools } from "redux-devtools-extension";
import thunk from 'redux-thunk'

// importação dos reducers
import SignReducer from "./auth/auth.reducer"
import CarroReducer from "./carro/carro.reducer"
import ReservaReducer from "./reserva/reserva.reducer"
import EstacionamentoReducer from "./estacionamento/estacionamento.reducer"
import UsuarioReducer from "./usuario/usuario.reducer"

const reducers = combineReducers({
    auth: SignReducer,
    carro: CarroReducer,
    reserva: ReservaReducer,
    usuario:UsuarioReducer,
    estacionamento:EstacionamentoReducer
})

// middlewares de redux
const middlewares = [thunk]

// compose junta os middlewares e ferramentas de debug

const compose = composeWithDevTools(applyMiddleware(...middlewares))

// criar a store do redux 

const store = createStore(reducers, compose)


export default store;