// import { saveAuth } from "../../config/auth";
// import { authService } from "../../services/auth.service";
// import history from '../../config/history';
import {postReservaInsert, updateReserva, delReserva,getReserva} from "../../services/res.service";

const reservaKey = 'reserva_edit'

export const TYPES = {
    RESERVA_LOADING: "RESERVA_LOADING",
    RESERVA_INSERT: "RESERVA_INSERT",
    RESERVA_SET: "RESERVA_SET",
    RESERVA_REFRESH: "RESERVA_REFRESH",
    RESERVA_UPDATE: "RESERVA_UPDATE",
    RESERVA_DELETE: "RESERVA_DELETE",
    RESERVA_ERROR:"RESERVA_ERROR",
    RESERVA_FINISH:"RESERVA_FINISH"

}

export const setReservaAction = (data,status) => {
    return async (dispatch) => {
        // dispatch({ type: TYPES.RESERVA_LOADING, status: true })
        try {
            dispatch({
                type: TYPES.RESERVA_SET,
                data : data, valid: status
            })
        } catch (error) {
            dispatch({ type: TYPES.RESERVA_ERROR, data: error.data })
        }
    }
}
export const finishReservaAction = () => {
    return async (dispatch) => {
        // dispatch({ type: TYPES.RESERVA_LOADING, status: true })
        try {
            dispatch({
                type: TYPES.RESERVA_FINISH,
            })
        } catch (error) {
        //    dispatch({ type: TYPES.RESERVA_ERROR, data: error.data })
        }
    }
}
export const postReservaAction = (data) => {
    return async (dispatch) => {
        dispatch({ type: TYPES.RESERVA_LOADING, status: true })
        try {
            delete data['createdAt']
            delete data['usuarioId']
            delete data['updatedAt']    
            const res = await postReservaInsert(data)
            dispatch({
                type: TYPES.RESERVA_INSERT,
                data:res.data
            })
        } catch (error) {
            dispatch({ type: TYPES.RESERVA_ERROR, data: error.data })
        }
        
    }
}
export const deleteReservaAction = (id) => {
    return async (dispatch) => {
        dispatch({ type: TYPES.RESERVA_LOADING, status: true })
        try {
            await delReserva({id})
            dispatch({
                type: TYPES.RESERVA_DELETE
            })
        } catch (error) {
            dispatch({ type: TYPES.RESERVA_ERROR, data: error.data })
        }
        
    }
}
export const refreshReservaAction = () => {
    return async (dispatch) => {
        dispatch({ type: TYPES.RESERVA_LOADING, status: true })
        try {
            const res = await getReserva()
            dispatch({
                type: TYPES.RESERVA_REFRESH,
                data: res.data
            })
        } catch (error) {
            dispatch({ type: TYPES.RESERVA_ERROR, data: error })
        }
    }
}

export const updateReservaAction = (id,data) => {
    return async (dispatch) => {
        dispatch({ type: TYPES.RESERVA_LOADING, status: true })
        try {

            var json = {find:{id},update:{
                ...data
            }}
            const res = await updateReserva(json)
            dispatch({
                type: TYPES.RESERVA_UPDATE,
                data: res.data
            })
        } catch (error) {
            dispatch({ type: TYPES.RESERVA_ERROR, data: error.data })
        }
    }
}





