import { TYPES } from './reserva.action'
import service from "../../services/car.service";
const INITIAL_STATE = {
    reservas: [],
    reserva: {},
    reservaCache: {},
    loading: false,
    isUpdate:false,
    registred:false,
    isDelete:false,
    refresh:null,
    isValid:true,
    error: []
};

const reducer =  (state = INITIAL_STATE, action) => { // tamara recebe
    switch (action.type) {
        case TYPES.RESERVA_LOADING:
        state.loading = action.status
        state.error = []
        return state;
        case TYPES.RESERVA_UPDATE:
        state.isUpdate = true
        state.isDelete = false
        state.loading = false
        state.refresh = true
        state.isValid = false
        state.reservaCache = {}
        return state;
        case TYPES.RESERVA_SET:
        state.reservaCache = action.data
        state.isValid = action.valid
        state.refresh = true
        return state;
        case TYPES.RESERVA_INSERT:
        state.isUpdate = false
        state.loading = false
        state.isDelete = false
        state.registred = true
        state.refresh = true
        state.isValid = false
        state.reservaCache = {}
        return state;
        case TYPES.RESERVA_DELETE:
        state.isUpdate = false
        state.loading = false
        state.isDelete = true
        state.refresh = true
        return state;
        case TYPES.RESERVA_FINISH:
        state.reservaCache = {}
        state.error = []
        state.isValid = true
        return state;
        case TYPES.RESERVA_REFRESH:
        state.reservas = action.data
        state.loading = false
        state.refresh = true
        return state;
        case TYPES.RESERVA_ERROR:
        const err = [action.data]
        state.loading = false
        state.isValid = true
        state.error = err;
        return state;
        default:
        return state;
    }
};

export default reducer;
