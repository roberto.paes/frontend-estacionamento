import { TYPES } from './usuario.action'
const INITIAL_STATE = {
    usuarios: [],
    usuarioCache:{},
    loading: false,
    isUpdate:false,
    isDelete:false,
    refresh:null,
    isValid:true,
    registred:false,
    error: []
};

const reducer =  (state = INITIAL_STATE, action) => { // tamara recebe
    switch (action.type) {
        case TYPES.USUARIO_LOADING:
        state.loading = action.status
        //state.error = []
        state.isValid = false
        
        return state;
        case TYPES.USUARIO_SET:
        state.usuarioCache = action.data
        state.isValid = action.valid
        state.loading = false
        return state;
        case TYPES.USUARIO_UPDATE:
        state.isUpdate = true
        state.isDelete = false
        state.loading = false
        state.isValid = true
        state.error = []
        state.usuarioCache = {}
        return state;
        case TYPES.USUARIO_INSERT:
        state.usuario = action.data
        state.isUpdate = false
        state.loading = false
        state.isDelete = false
        state.isValid = true
        state.error = []
        state.usuarioCache = {}
        return state;
        case TYPES.USUARIO_DELETE:
        state.isUpdate = false
        state.loading = false
        state.isDelete = true
        state.error = []
        state.isValid = true
        return state;
        case TYPES.USUARIO_REFRESH:
        state.usuarios = action.data
        state.loading = false
        return state;
        case TYPES.USUARIO_ERROR:
        const err = [...state.error, action.data]
        state.loading = false
        state.error = err;
        return state;
        default:
        return state;
    }
};

export default reducer;
