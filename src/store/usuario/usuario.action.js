// import { saveAuth } from "../../config/auth";
// import { authService } from "../../services/auth.service";
// import history from '../../config/history';
import { removeToken, removeUser, saveAuth, saveUser } from "../../config/auth";

import {authUpdate,getUser} from "../../services/user.service";
import store from '../../store';


export const TYPES = {
    USUARIO_LOADING: "USUARIO_LOADING",
    USUARIO_SET: "USUARIO_SET",
    USUARIO_REFRESH: "USUARIO_REFRESH",
    USUARIO_UPDATE: "USUARIO_UPDATE",
    USUARIO_INSERT: "USUARIO_INSERT",
    USUARIO_DELETE: "USUARIO_DELETE",
    USUARIO_ERROR:"USUARIO_ERROR"
}

export const setUsuarioAction = (data,status) => {
    return async (dispatch) => {
        //   dispatch({ type: TYPES.USUARIO_LOADING, status: true })
        try {
            
            dispatch({
                type: TYPES.USUARIO_SET,
                data : data, valid: status
            })
            console.log('car action',data,status)
        } catch (error) {
            dispatch({ type: TYPES.USUARIO_ERROR, data: error })
        }
    }
}
export const refreshUsuarioAction = () => {
    return async (dispatch) => {
        dispatch({ type: TYPES.USUARIO_LOADING, status: true })
        try {
            const state = store.getState();
            
            const user = await getUser()
            removeUser()
            saveUser({nome:user.data.nome,tipo:user.data.tipo})
            state.auth.isLogged = true
            state.auth.usuario = user.data
            
            dispatch({
                type: TYPES.USUARIO_REFRESH
            })
        } catch (error) {
            dispatch({ type: TYPES.USUARIO_ERROR, data: error })
        }
        
    }
}
export const updateUsuarioction = (data) => {
    return async (dispatch) => {
        dispatch({ type: TYPES.USUARIO_LOADING, status: true })
        try {
            delete data['createdAt']
            delete data['usuarioId']
            delete data['updatedAt']
            var json = {update:{
                ...data
            }}
            
            var res = await authUpdate(json)
            dispatch({
                type: TYPES.USUARIO_UPDATE,
                data: res.data
            })
        } catch (error) {
            dispatch({ type: TYPES.USUARIO_ERROR, data: error })
        }
        
    }
}








