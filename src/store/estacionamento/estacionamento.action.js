// import { saveAuth } from "../../config/auth";
// import { authService } from "../../services/auth.service";
// import history from '../../config/history';
import http from '../../config/http'

import { getsearchEstacionamento, getEstacionamentosList,getEstacionamentoAll,getEstacionamentoDetalhe,delEstacionamento,updateEstacionamento,postEstacionamento,getByEstado,getEstacionamentoId,getEstacionamento} from "../../services/est.service";


export const estKey = 'est_edit'

export const TYPES = {
    ESTACIONAMENTO_LOADING: "ESTACIONAMENTO_LOADING",
    ESTACIONAMENTO_INSERT: "ESTACIONAMENTO_INSERT",
    ESTACIONAMENTO_COUNTRY: "ESTACIONAMENTO_COUNTRY",
    ESTACIONAMENTO_FINISH: "ESTACIONAMENTO_FINISH",
    ESTACIONAMENTO_GET: "ESTACIONAMENTO_GET",
    ESTACIONAMENTO_DETALHE: "ESTACIONAMENTO_DETALHE",
    ESTACIONAMENTO_REFRESH: "ESTACIONAMENTO_REFRESH",
    ESTACIONAMENTO_UPDATE: "ESTACIONAMENTO_UPDATE",
    ESTACIONAMENTO_DELETE: "ESTACIONAMENTO_DELETE",
    ESTACIONAMENTO_ERROR:"ESTACIONAMENTO_ERROR",
    ESTACIONAMENTO_SET:"ESTACIONAMENTO_SET"
}
export const finishEstacionamentoAction = () => {
    return async (dispatch) => {
        // dispatch({ type: TYPES.ESTACIONAMENTO_LOADING, status: true })
        try {
            dispatch({
                type: TYPES.ESTACIONAMENTO_FINISH,
            })
        } catch (error) {
          //  dispatch({ type: TYPES.ESTACIONAMENTO_ERROR, data: error.data })
        }
    }
}

export const setEstacionamentoAction = (result,status) => {
    return async (dispatch) => {
        // dispatch({ type: TYPES.ESTACIONAMENTO_LOADING, status: true })
        try {
            dispatch({
                type: TYPES.ESTACIONAMENTO_SET,
                data : result, valid: status
            })
       console.log('test',JSON.stringify(result))
        } catch (error) {
        //    dispatch({ type: TYPES.ESTACIONAMENTO_ERROR, data: error.data })
        }
    }
}
export  const postEstacionamentoAction = (data) => {
    return async (dispatch) => {
        dispatch({ type: TYPES.ESTACIONAMENTO_LOADING, status: true })
        try {
            delete data['createdAt']
            delete data['usuarioId']
            delete data['updatedAt']    
            console.log('parou antes')
            const res = await postEstacionamento(data)
            console.log('executou até aqui')
            dispatch({
                type: TYPES.ESTACIONAMENTO_INSERT,
                data:res.data
            })
        } catch (error) {
            console.log('entrou no catch')
            dispatch({ type: TYPES.ESTACIONAMENTO_ERROR, data: error.data })
        }
        
    }
}
export const deleteEstacionamentoAction = (id) => {
    return async (dispatch) => {
        dispatch({ type: TYPES.ESTACIONAMENTO_LOADING, status: true })
        try {
            const res = await delEstacionamento({id})
            dispatch({
                type: TYPES.ESTACIONAMENTO_DELETE,
                data: res.data
            })
        } catch (error) {
            dispatch({ type: TYPES.ESTACIONAMENTO_ERROR, data: error.data })
        }
    }
}
export const searchEstacionamento = (cep) => {
    return async (dispatch) => {
        dispatch({ type: TYPES.ESTACIONAMENTO_LOADING, status: true })
        try {
           
            
         const res = await getsearchEstacionamento(cep)
            dispatch({
                type: TYPES.ESTACIONAMENTO_DETALHE,
                data: res.data
            })
        } catch (error) {
            dispatch({ type: TYPES.ESTACIONAMENTO_ERROR, data: error.data })
        }
    }
}
export const getEstacionamentoDetalheAction= (estado,cidade,bairro) => {
    return async (dispatch) => {
        dispatch({ type: TYPES.ESTACIONAMENTO_LOADING, status: true })
        try {
            var res = undefined
            
            if(!cidade && !bairro){
                res = await http.get(`/v1/estacionamento/lista/${estado}`)
                
            }else if (!bairro){
                res = await http.get(`/v1/estacionamento/lista/${estado}/${cidade}`)
                
            }else{
                res = await http.get(`/v1/estacionamento/lista/${estado}/${cidade}/${bairro}`)
            }
            dispatch({
                type: TYPES.ESTACIONAMENTO_DETALHE,
                data: res.data
            })
        } catch (error) {
            dispatch({ type: TYPES.ESTACIONAMENTO_ERROR, data: error.data })
        }
    }
}
export const getEstacionamentoAction= (id) => {
    return async (dispatch) => {
        dispatch({ type: TYPES.ESTACIONAMENTO_LOADING, status: true })
        try {
            const res = await getEstacionamentoId(id)
            dispatch({
                type: TYPES.ESTACIONAMENTO_GET,
                data: res.data
            })
        } catch (error) {
            dispatch({ type: TYPES.ESTACIONAMENTO_ERROR, data: error })
        }
    }
}
export const getEstacionamentoAllAction= () => {
    return async (dispatch) => {
        dispatch({ type: TYPES.ESTACIONAMENTO_LOADING, status: true })
        try {
            const res = await getEstacionamentoAll()
            dispatch({
                type: TYPES.ESTACIONAMENTO_DETALHE,
                data: res.data
            })
        } catch (error) {
            dispatch({ type: TYPES.ESTACIONAMENTO_ERROR, data: error })
        }
    }
}
export const getEstacionamentoListaAction= () => {
    return async (dispatch) => {
        dispatch({ type: TYPES.ESTACIONAMENTO_LOADING, status: true })
        try {
            const res = await getEstacionamentosList()
            dispatch({
                type: TYPES.ESTACIONAMENTO_COUNTRY,
                data: res.data
            })
        } catch (error) {
            dispatch({ type: TYPES.ESTACIONAMENTO_ERROR, data: error })
        }
    }
}
export const refreshEstacionamentoAction = () => {
    return async (dispatch) => {
        dispatch({ type: TYPES.ESTACIONAMENTO_LOADING, status: true })
        try {
            const res = await getEstacionamento()
            dispatch({
                type: TYPES.ESTACIONAMENTO_REFRESH,
                data: res.data
            })
        } catch (error) {
            dispatch({ type: TYPES.ESTACIONAMENTO_ERROR, data: error.data })
        }
    }
}

export const updateEstacionamentoAction = (id,data) => {
    return async (dispatch) => {
        dispatch({ type: TYPES.ESTACIONAMENTO_LOADING, status: true })
        try {
            
            
            delete data['createdAt']
            delete data['usuarioId']
            delete data['updatedAt']
            delete data['id']    
            delete data['vaga_ocupada']       
            data['cep'] = data['cep'].replace(/[^\d]/g, "");

             
            var json = {find:{id},update:{
                ...data
            }}
            
            const res = await updateEstacionamento(json)
            
            dispatch({
                type: TYPES.ESTACIONAMENTO_UPDATE,
                data: res.data
            })
        } catch (error) {
            dispatch({ type: TYPES.ESTACIONAMENTO_ERROR, data: error.data })
        }
    }
}





