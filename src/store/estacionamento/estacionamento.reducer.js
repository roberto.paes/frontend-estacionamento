import { TYPES,estKey } from './estacionamento.action'
import service from "../../services/car.service";
const INITIAL_STATE = {
    estacionamentos: [],
    estacionamentosUsuario: [],
    estacionamento: {},
    estacionamentoCache : {},
    estacionamentoCountry : [],
    loading: false,
    registred:false,
    isUpdate:false,
    isDelete:false,
    refresh:null,
    isValid:true,
    error: []
};

const reducer =  (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case TYPES.ESTACIONAMENTO_LOADING:
        state.loading = action.status
        state.error = []
        return state;
        case TYPES.ESTACIONAMENTO_UPDATE:
        state.isUpdate = true
        state.isDelete = false
        state.loading = false
        state.refresh = true
        state.registred = false
        state.isValid = false
        state.estacionamentoCache = {}
        return state;
        case TYPES.ESTACIONAMENTO_INSERT:
        state.isUpdate = false
        state.loading = false
        state.isDelete = false
        state.registred = true
        state.refresh = true
        state.isValid = false
        //    state.error = []
        state.estacionamentoCache = {}
        return state;
        case TYPES.ESTACIONAMENTO_GET:
        state.estacionamento = action.data
        state.isUpdate = false
        state.loading = false
        state.isDelete = true
        state.refresh = true
        return state;
        case TYPES.ESTACIONAMENTO_DELETE:
        state.isUpdate = false
        state.loading = false
        state.isDelete = true
        state.refresh = true
        //   state.isValid = true
        //      state.error = []
        return state;
        case TYPES.ESTACIONAMENTO_FINISH:
        state.estacionamentoCache = {}
        state.error = []
        state.isValid = true
        //     state.isValid = false
        
        //   state.error = []
        return state;
        case TYPES.ESTACIONAMENTO_SET:
        state.estacionamentoCache = action.data
        state.isValid = action.valid
        state.loading = false
        return state;
        case TYPES.ESTACIONAMENTO_COUNTRY:
        state.estacionamentoCountry = action.data
        state.loading = false
        state.refresh = true
        return state;
        case TYPES.ESTACIONAMENTO_REFRESH:
        state.estacionamentosUsuario = action.data
        //   state.estacionamentoCache = {}
        
        state.loading = false
        state.refresh = true
        return state;
        case TYPES.ESTACIONAMENTO_DETALHE:
        state.estacionamentos = action.data
        state.loading = false
        state.refresh = true
        return state;
        case TYPES.ESTACIONAMENTO_ERROR:
        const err = [action.data]
        state.isValid = true
        state.loading = false
        state.error = err;
        return state;
        default:
        return state;
    }
};

export default reducer;
