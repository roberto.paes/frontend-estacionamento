import { removeToken, removeUser, saveAuth, saveUser } from "../../config/auth";
import { authLogin, authRegister, getUser } from "../../services/user.service";
import history from '../../config/history';
import http from '../../config/http';


export const TYPES = {
    SIGN_IN: "SIGN_IN",
    SIGN_UP: "SIGN_UP",
    SIGN_OUT: "SIGN_OUT",
    SIGN_ERROR: "SIGN_ERROR",
    SIGN_LOADING: "SIGN_LOADING",
    
}

export const signInAction = (data) => {
    return async (dispatch) => {
        dispatch({ type: TYPES.SIGN_LOADING, status: true })
        
        try {
            const result = await authLogin(data) 
            if (result.data) {
                
                saveAuth(result.data)
                http.defaults.headers['x-access-token'] = result.data.token;
                const user = await getUser() 
                saveUser({nome:user.data.nome,tipo:user.data.tipo})
                
            }
            dispatch({
                type: TYPES.SIGN_IN, data: result.data,usuario:getUser()  
            })
        } catch (error) {
            dispatch({ type: TYPES.SIGN_ERROR, data: error.data })
        }
        
    };
}
export const signUpAction = (data) => {
    
    return async (dispatch) => {
        dispatch({ type: TYPES.SIGN_LOADING, status: true })
        
        try {
            await authRegister(data)
            dispatch({
                type: TYPES.SIGN_UP
            })
        } catch (error) {
            
            dispatch({ type: TYPES.SIGN_ERROR, data: error.data })
        }
        
    };
}
export const logoutAction = () => {
    
    return async (dispatch) => {
        removeUser()
        removeToken()
        dispatch({ type: TYPES.SIGN_OUT })
   
        
        
    };
}




