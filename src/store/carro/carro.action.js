// import { saveAuth } from "../../config/auth";
// import { authService } from "../../services/auth.service";
// import history from '../../config/history';
import {getCarro,getCarros,editCarro,deleteCarro,postCarro} from "../../services/car.service";
const carroKey = 'carro'

export const TYPES = {
    CARRO_LOADING: "CARRO_LOADING",
    CARRO_SET: "CARRO_SET",
    CARRO_REFRESH: "CARRO_REFRESH",
    CARRO_UPDATE: "CARRO_UPDATE",
    CARRO_INSERT: "CARRO_INSERT",
    CARRO_DELETE: "CARRO_DELETE",
    CARRO_ERROR:"CARRO_ERROR",  
    CARRO_FINISH:"CARRO_FINISH"
    
}
export const finishCarroAction = () => {
    return async (dispatch) => {
        // dispatch({ type: TYPES.ESTACIONAMENTO_LOADING, status: true })
        try {
            dispatch({
                type: TYPES.CARRO_FINISH,
            })
        } catch (error) {
            //       dispatch({ type: TYPES.CARRO_ERROR, data: error })
        }
    }
}

export const setCarroAction = (data,status) => {
    return async (dispatch) => {
        //   dispatch({ type: TYPES.CARRO_LOADING, status: true })
        try {
            
            dispatch({
                type: TYPES.CARRO_SET,
                data : data, valid: status
            })
            console.log('car action',data,status)
        } catch (error) {
            //           dispatch({ type: TYPES.CARRO_ERROR, data: error })
        }
    }
}
export const postCarroAction = (data) => {
    return async (dispatch) => {
        dispatch({ type: TYPES.CARRO_LOADING, status: true })
        try {
            delete data['createdAt']
            delete data['usuarioId']
            delete data['updatedAt']    
            const res = await postCarro(data)
            dispatch({
                type: TYPES.CARRO_INSERT,
                data:res.data
            })
        } catch (error) {
            dispatch({ type: TYPES.CARRO_ERROR, data: error.data })
        }
        
    }
}
export const refreshCarAction = () => {
    return async (dispatch) => {
        dispatch({ type: TYPES.CARRO_LOADING, status: true })
        try {
            const carros = await getCarros()
            dispatch({
                type: TYPES.CARRO_REFRESH,
                data:carros.data
            })
        } catch (error) {
            dispatch({ type: TYPES.CARRO_ERROR, data: error.data })
        }
        
    }
}
export const updateCarroAction = (id,data) => {
    return async (dispatch) => {
        dispatch({ type: TYPES.CARRO_LOADING, status: true })
        try {
            delete data['createdAt']
            delete data['usuarioId']
            delete data['updatedAt']
            var json = {find:{id},update:{
                ...data
            }}
            
            var res = await editCarro(json)
            dispatch({
                type: TYPES.CARRO_UPDATE,
                data: res.data
            })
        } catch (error) {
            dispatch({ type: TYPES.CARRO_ERROR, data: error.data })
        }
        
    }
}


export const deleteCarroAction= (id) => {
    return async (dispatch) => {
        dispatch({ type: TYPES.CARRO_LOADING, status: true })
        try {
            
            var res = await deleteCarro({id})
            console.log('try delete',id)
            dispatch({
                type: TYPES.CARRO_DELETE,
                data: res.data
            })
        } catch (error) {
            dispatch({ type: TYPES.CARRO_ERROR, data: error.data })
        }
    }
}





