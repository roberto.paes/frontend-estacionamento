import { TYPES } from './carro.action'
const INITIAL_STATE = {
    carros: [],
    carroCache:{},
    loading: false,
    isUpdate:false,
    isDelete:false,
    refresh:null,
    isValid:true,
    registred:false,
    error: []
};

const reducer =  (state = INITIAL_STATE, action) => { // tamara recebe
    switch (action.type) {
        case TYPES.CARRO_LOADING:
        state.loading = action.status
        state.error = []
        return state;
        case TYPES.CARRO_SET:
        state.carroCache = action.data
        state.isValid = action.valid
        state.loading = false
        return state;
        case TYPES.CARRO_UPDATE:
        state.isUpdate = true
        state.isDelete = false
        state.loading = false
        state.isValid = false
        
        state.carroCache = {}
        return state;
        case TYPES.CARRO_INSERT:
        state.carro = action.data
        state.isUpdate = false
        state.loading = false
        state.isDelete = false
        state.isValid = false
        
        state.carroCache = {}
        return state;
        case TYPES.CARRO_FINISH:
        state.carroCache = {}
        state.error = []
        state.isValid = true
        return state;
        case TYPES.CARRO_DELETE:
        state.isUpdate = false
        state.loading = false
        state.isDelete = true
        return state;
        case TYPES.CARRO_REFRESH:
        state.carros = action.data
        state.loading = false
        return state;
        case TYPES.CARRO_ERROR:
        const err = [action.data]
        state.loading = false
        state.error = err;
        state.isValid = true
        
        return state;
        default:
        return state;
    }
};

export default reducer;
