

const TOKEN_KEY = 'auth_token'
const USER_KEY = 'auth_user'

const getToken = () => {
    const data = JSON.parse(localStorage.getItem(TOKEN_KEY));
    if (data && data.token) {
        return data.token; // TODO:  geralmente é token
    }
    return false;
};

const getUserId= () => {
    const data = JSON.parse(localStorage.getItem(TOKEN_KEY));
    if (data && data.usuario) {
        return data.usuario;
    }
    return false;
};

const getUser= () => {
    const data = JSON.parse(localStorage.getItem(USER_KEY));
    if (data) {
        return data;
    }
    return false;
};
const isAuthenticated = () => {
    // pegar dentro do localstage
    // validar o token
    // retornar se true ou false
    return getToken() !== false;
};

const removeToken = () => localStorage.removeItem(TOKEN_KEY);
const removeUser= () => localStorage.removeItem(USER_KEY);

const saveAuth = (data) => localStorage.setItem(TOKEN_KEY, JSON.stringify(data))
const saveUser = (data) => localStorage.setItem(USER_KEY, JSON.stringify(data))


export {
    saveAuth,
    removeUser,
    getUser,
    saveUser,
    getToken,
    getUserId,
    isAuthenticated,
    removeToken
}
