import { getToken, removeToken, removeUser } from './auth';
import axios from 'axios'; // import da dependencia
import history from './history';
import { logoutAction } from '../store/auth/auth.action'
import store from '../store'

// definindo a url da api
const urlApi = process.env.REACT_APP_URLAPI || 'http://127.0.0.1:8000'
// const urlApi = "https://projeto-02-backend.herokuapp.com/v1";

// criando um client http através do AXIOS
const http = axios.create({
    baseURL: urlApi
});

// Definindo o header padrão da aplicação
http.defaults.headers['Content-Type'] = 'application/json';
if (getToken()) {
    http.defaults.headers['x-access-token'] = getToken();
}
http.interceptors.response.use((response) => {
    return response;
}, function (error) {
    // Do something with response error
    switch(error.response.status) {
        case 401:
        store.dispatch(logoutAction())
       //history.push('/login')
       break
       case 500:
        store.dispatch(logoutAction())
     //   history.push('/login')
        break
    }
    return Promise.reject(error.response);
});

// http.interceptors.response.use(
//     (response) => response,
//     (error) => {
//         switch (error.response.status) {
//             case 401:
//                 store.dispatch(logoutAction())
//                 history.push('/login')
//                 break;
//             default:
//                 break;
//         }
//     }
 
// )






export default http;
